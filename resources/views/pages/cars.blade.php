@extends('layouts.default')

@section('content')
    <style type="text/css" media="print">
        body {
            visibility: hidden;
            display: none
        }
    </style>
    <div class="row">
        <div class="col-xs-12">
            <h2>Public Car Listing</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-right">
            {{--<a href="{{ url('/export/pdf') }}" target="_blank">--}}
                {{--<button class="btn btn-info">--}}
                    {{--<i class="fa fa-download"></i> Export to Wholesale PDF--}}
                {{--</button>--}}
            {{--</a>--}}
            <a href="{{ url('/export/html?sort=u_date') }}" target="_blank">
                <button class="btn btn-info">
                    <i class="fa fa-download"></i> Export to Wholesale HTML (Latest)
                </button>
            </a>
            <a href="{{ url('/export/html') }}" target="_blank">
                <button class="btn btn-info">
                    <i class="fa fa-download"></i> Export to Wholesale HTML (Default)
                </button>
            </a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered sc-datatable column-search public-listing">
                <thead>
                <tr class="secondary">
                    <th class="sc-no">No.</th>
                    @role('admin')
                    <th>Dealer</th>
                    <th>Chassis No.</th>
                    @endrole
                    <th>Unique No.</th>
                    <th>YOM</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>VAC</th>
                    <th>OMV</th>
                    <th>eVES/VES</th>
                    <th>Colour</th>
                    @role('admin')
                    <th>Dealer Price</th>
                    @endrole
                    <th>Wholesale Price</th>
                    <th>Type</th>
                    <th>Remarks</th>
                    <th>Sold</th>
                    <th class="sc-u-date">U. Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cars as $index => $car)
                    @php
                        $yom_class = '';
                        $yom = $car->yom;
                        $cur_year = date('Y');

                        if ($yom) {
                            if ($cur_year <= $yom) {
                                $yom_class = 'current';
                            } else if ($cur_year - $yom >= 2) {
                                $yom_class = 'old';
                            }
                        }

                        if (!empty((float) $car->price)) {
                            $dealer_price = number_format($car->price);
                            $wholesale_price = number_format(getWholesalePrice($car->price));
                        } else {
                            $dealer_price = '';
                            $wholesale_price = '';
                        }
                    @endphp
                    <tr>
                        <td>{{ $car->listing_no }}</td>
                        @role('admin')
                        <td>{{ $car->company }}</td>
                        <td>{{ $car->chassis_no }}</td>
                        @endrole
                        <td>{{ $car->id }}</td>
                        <td class="{{ $yom_class }}">{{ $car->yom }}</td>
                        <td>{{ $car->make }}</td>
                        <td>{{ $car->model }}</td>
                        <td>{{ substr($car->vac, 0, 1) }}</td>
                        <td style="text-align: right;">{{ format_value('omv', number_format($car->omv)) }}</td>
                        <td>{{ $car->cevs }}</td>
                        <td>{{ $car->colour }}</td>
                        @role('admin')
                        <td style="text-align: right;"><strong>{{ $dealer_price }}</strong></td>
                        @endrole
                        <td style="text-align: right;"><strong>{{ $wholesale_price }}</strong></td>
                        <td>{{ format_value('type', $car->type) }}</td>
                        <td class="remarks">{{ $car->remarks }}</td>
                        <td @if(!empty($car->sold)) {!! 'class="sold"' !!} @endif>{{ $car->sold }}</td>
                        <td>{{ date_format(date_create($car->updated_at), 'd/m/Y') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop