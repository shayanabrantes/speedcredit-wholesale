@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h2>Manage Dealers</h2>
        </div>
    </div>

    @if(session()->has('success'))
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            </div>
        </div>
    @endif

    @if(count($dealers) > 0)
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-bordered sc-datatable">
                    <thead>
                    <tr>
                        <th class="sc-no">No.</th>
                        <th>Company</th>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th class="sc-no-sort">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dealers as $index => $dealer)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$dealer->company}}</td>
                            <td>{{$dealer->name}}</td>
                            <td>{{$dealer->email}}</td>
                            <td>
                                <a href="{{ url('/admin/dealers/' . $dealer->id . '/edit') }}"
                                   class="btn btn-info">Edit</a>

                                {{ Form::open(array('url' => 'admin/dealers/' . $dealer->id)) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <button type="button" class="btn btn-danger dealer-delete" id="car-{{ $dealer->id }}">Delete</button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <p>You don't have any dealers yet.
    @endif

    <script>
        (function($){
            $(function () {
                $('.dealer-delete').on('click', function(){
                    var result = confirm('Are you sure you want to delete this dealer? All cars under this dealer will also be deleted.');

                    if(result == true){
                        $(this).parent('form').submit();
                    }
                })
            });
        })(jQuery);
    </script>
@stop

