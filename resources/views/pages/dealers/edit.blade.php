@extends('layouts.default')

@section('content')
    @if(session()->has('success'))
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            </div>
        </div>
    @endif

    <div class="clearfix"></div>
            <div class="x_panel">
                <div class="x_title">
                    @if(Request::is('profile') || Request::is('admin/profile'))
                        <h2>Edit Profile</h2>
                    @else
                        <h2>Edit Dealer</h2>
                    @endif
                    <div class="clearfix"></div>
                </div>
        <div class="row">
            <div class="col-xs-2">
                <ul class="nav nav-tabs tabs-left">
                    <li class="active"><a href="#profile" data-toggle="tab" aria-expanded="true">Profile</a>
                    </li>
                    <li class=""><a href="#notifications" data-toggle="tab" aria-expanded="false">Notifications</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-10">
                    @if(Request::is('profile'))
                        {{ Form::model($dealer, ['route' => 'dealer.profile.save']) }}
                    @elseif(Request::is('admin/profile'))
                        {{ Form::model($dealer, ['route' => 'admin.profile.save']) }}
                    @else
                        {{ Form::model($dealer, ['route' => ['admin.dealers.update', $dealer->id]]) }}
                        <input name="_method" type="hidden" value="PUT">
                    @endif
                    <input name="id" type="hidden" value="{{ $dealer->id }}">
                <div class="tab-content">
                    <div class="tab-pane active" id="profile">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {{ Form::label('email', 'Email Address', ['class' => 'control-label form-required']) }}
                                {{ Form::text('email', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                    {{ $errors->first('email') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                                {{ Form::label('company', 'Company', ['class' => 'control-label form-required']) }}
                                @if (Auth::user()->roles->first()->name == 'dealer')
                                    {{ Form::text('company', null , array('class' => 'form-control', 'readonly' => 'readonly')) }}
                                @elseif (Auth::user()->roles->first()->name == 'admin')
                                    {{ Form::text('company', null , array('class' => 'form-control')) }}
                                @endif
                                <span class="help-block">
                                {{ $errors->first('company') }}
                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                {{ Form::label('name', 'Name', ['class' => 'control-label form-required']) }}
                                {{ Form::text('name', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                {{ $errors->first('name') }}
                            </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('fax') ? ' has-error' : '' }}">
                                {{ Form::label('fax', 'Fax', ['class' => 'control-label']) }}
                                {{ Form::number('fax', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                {{ $errors->first('fax') }}
                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('mobile_no') ? ' has-error' : '' }}">
                                {{ Form::label('mobile_no', 'Mobile Number', ['class' => 'control-label']) }}
                                <div class="input-group">
                                    <div class="input-group-addon">+65</div>
                                    {{ Form::number('mobile_no', null , array('class' => 'form-control')) }}
                                </div>
                        <span class="help-block">
                                {{ $errors->first('mobile_no') }}
                            </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('tel_no') ? ' has-error' : '' }}">
                                {{ Form::label('tel_no', 'Telephone Number', ['class' => 'control-label']) }}
                                <div class="input-group">
                                    <div class="input-group-addon">+65</div>
                                    {{ Form::number('tel_no', null , array('class' => 'form-control')) }}
                                </div>
                        <span class="help-block">
                                {{ $errors->first('tel_no') }}
                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
                                {{ Form::label('address1', 'Address Line 1', ['class' => 'control-label']) }}
                                {{ Form::text('address1', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                {{ $errors->first('address1') }}
                            </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}">
                                {{ Form::label('address2', 'Address Line 2', ['class' => 'control-label']) }}
                                {{ Form::text('address2', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                {{ $errors->first('address2') }}
                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                {{ Form::label('postal_code', 'Postal Code', ['class' => 'control-label']) }}
                                {{ Form::text('postal_code', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                {{ $errors->first('postal_code') }}
                            </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('business_reg_no') ? ' has-error' : '' }}">
                                {{ Form::label('business_reg_no', 'Business Registration Number', ['class' => 'control-label']) }}
                                {{ Form::text('business_reg_no', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                {{ $errors->first('business_reg_no') }}
                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                {{ Form::label('password', 'Password', ['class' => 'control-label']) }}
                                {{ Form::password('password', array('class' => 'form-control')) }}
                                <span class="help-block">
                                    {{ $errors->first('password') }}
                                    (Leave blank if no changes will be made)
                                </span>
                            </div>
                        </div>
                    </div>

                    @if (!Request::is('profile') && !Request::is('admin/profile'))
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="checkbox" id="send_email" name="send_email" checked="checked" value="1"/> <label for="send_email">Email user credentials (when updating password only)</label>
                        </div>
                    </div>
                    @endif


                    <p class="form-required-text">Required Fields</p>
                    </div>
                    <div class="tab-pane" id="notifications">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group{{ $errors->has('email_frequency') ? ' has-error' : '' }}">
                                    {{ Form::radio('email_frequency', 'none', true, ['id' => 'none']) }}
                                    {{ Form::label('none', 'No, I don\'t want to receive any email.', ['class' => 'control-label']) }}
                                    <br/>
                                    {{ Form::radio('email_frequency', 'daily', true, ['id' => 'daily']) }}
                                    {{ Form::label('daily', 'Yes, daily.', ['class' => 'control-label']) }}
                                    <br/>
                                    {{ Form::radio('email_frequency', 'weekly', true, ['id' => 'weekly']) }}
                                    {{ Form::label('weekly', 'Yes, weekly.', ['class' => 'control-label']) }}
                                    <br/>
                                    {{ Form::radio('email_frequency', 'minute', true, ['id' => 'minute']) }}
                                    {{ Form::label('minute', 'TEST - EVERY MINUTE.', ['class' => 'control-label']) }}
                                    <span class="help-block">
                                        {{ $errors->first('email_frequency') }}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <button type="submit" class="btn btn-success pull-right">Update</button>
                    {{ Form::close() }}
            </div>
        </div>
    </div>
@stop