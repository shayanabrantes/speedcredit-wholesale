@extends('layouts.default')

@section('content')
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Dealer</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    {{ Form::open(array('route' => 'admin.dealers.store')) }}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {{ Form::label('email', 'Email Address', ['class' => 'control-label form-required']) }}
                                {{ Form::text('email', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                    {{ $errors->first('email') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                                {{ Form::label('company', 'Company Name', ['class' => 'control-label form-required']) }}
                                {{ Form::text('company', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                    {{ $errors->first('company') }}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                {{ Form::label('name', 'Name', ['class' => 'control-label form-required']) }}
                                {{ Form::text('name', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                    {{ $errors->first('name') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('fax') ? ' has-error' : '' }}">
                                {{ Form::label('fax', 'Fax', ['class' => 'control-label']) }}
                                {{ Form::number('fax', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                    {{ $errors->first('fax') }}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('mobile_no') ? ' has-error' : '' }}">
                                {{ Form::label('mobile_no', 'Mobile Number', ['class' => 'control-label']) }}
                                <div class="input-group">
                                    <div class="input-group-addon">+65</div>
                                    {{ Form::number('mobile_no', null , array('class' => 'form-control')) }}
                                </div>
                                <span class="help-block">
                                    {{ $errors->first('mobile_no') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('tel_no') ? ' has-error' : '' }}">
                                {{ Form::label('tel_no', 'Telephone Number', ['class' => 'control-label']) }}
                                <div class="input-group">
                                    <div class="input-group-addon">+65</div>
                                    {{ Form::number('tel_no', null , array('class' => 'form-control')) }}
                                </div>
                                <span class="help-block">
                                    {{ $errors->first('tel_no') }}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
                                {{ Form::label('address1', 'Address Line 1', ['class' => 'control-label']) }}
                                {{ Form::text('address1', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                    {{ $errors->first('address1') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}">
                                {{ Form::label('address2', 'Address Line 2', ['class' => 'control-label']) }}
                                {{ Form::text('address2', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                    {{ $errors->first('address2') }}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                {{ Form::label('postal_code', 'Postal Code', ['class' => 'control-label']) }}
                                {{ Form::number('postal_code', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                    {{ $errors->first('postal_code') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('business_reg_no') ? ' has-error' : '' }}">
                                {{ Form::label('business_reg_no', 'Business Registration Number', ['class' => 'control-label']) }}
                                {{ Form::number('business_reg_no', null , array('class' => 'form-control')) }}
                                <span class="help-block">
                                    {{ $errors->first('business_reg_no') }}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                {{ Form::label('password', 'Password', ['class' => 'control-label form-required']) }}
                                {{ Form::password('password', array('class' => 'form-control')) }}
                                <span class="help-block">
                                        {{ $errors->first('password') }}
                                    </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="checkbox" id="send_email" name="send_email" checked="checked" value="1"/>
                            <label for="send_email">Email user credentials</label>
                        </div>
                    </div>

                    <p class="form-required-text">Required Fields</p>
                    <button type="submit" class="btn btn-success pull-right">Create Dealer</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop