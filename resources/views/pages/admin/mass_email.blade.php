@extends('layouts.default')

@section('content')
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Send Mass Email</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    @if(Session::has('success'))
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                </div>
                            </div>
                        </div>
                    @endif
                    {!! Form::open(['route'=>'admin.send-email.send', 'files' => 'true']) !!}
                    <div class="form-group {{ $errors->has('to') ? 'has-error' : '' }}">
                        {!! Form::label('to', 'To', ['class' => 'control-label form-required']) !!}
                        {!! Form::select('to', array(null  => 'Please select...', 'test_dev' => 'Test Contacts (Dev)', 'test' => 'Test Contacts (Jesse, Marc and Joanne)', 'temp' => 'Custom (Temporary)', 'all' => 'All Contacts (Be careful, this is live/working)'), null, ['class'=>'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('to') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
                        {!! Form::label('subject', 'Subject', ['class' => 'control-label form-required']) !!}
                        {!! Form::text('subject', $default_subject, ['class'=>'form-control', 'placeholder'=>'Subject']) !!}
                        <span class="text-danger">{{ $errors->first('subject') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('intro') ? 'has-error' : '' }}">
                        {!! Form::label('intro', 'Intro Message', ['class' => 'control-label']) !!}
                        {!! Form::textarea('intro', $settings->mail_default_intro, ['class'=>'form-control tiny-mce', 'placeholder'=>'Intro Message']) !!}
                        <span class="text-danger">{{ $errors->first('intro') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('wanted') ? 'has-error' : '' }}">
                        {!! Form::label('wanted', 'Wanted List', ['class' => 'control-label']) !!}
                        {!! Form::textarea('wanted', $settings->mail_default_wanted_list, ['class'=>'form-control tiny-mce', 'placeholder'=>'Wanted List']) !!}
                        <span class="text-danger">{{ $errors->first('wanted') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
                        {!! Form::label('message', 'Message', ['class' => 'control-label form-required']) !!}
                        {!! Form::textarea('message', $settings->mail_default_message, ['class'=>'form-control tiny-mce', 'placeholder'=>'Message']) !!}
                        <span class="text-danger">{{ $errors->first('message') }}</span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            {!! Form::label('Attachment 1:') !!}
                            {{ Form::file('attachment') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Attachment 2:') !!}
                            {{ Form::file('attachment_2') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Attachment 3:') !!}
                            {{ Form::file('attachment_3') }}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            {!! Form::label('Attachment 4:') !!}
                            {{ Form::file('attachment_4') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Attachment 5:') !!}
                            {{ Form::file('attachment_5') }}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        {{ Form::button('Send', array('type' => 'submit', 'class' => 'btn btn-success')) }}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop