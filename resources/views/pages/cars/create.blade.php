@extends('layouts.default')

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Add Car</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Car</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    @role('admin')
                        {{ Form::open(array('route' => 'admin.cars.store', 'class' => 'form-horizontal form-label-left')) }}
                    @endrole
                    @role('dealer')
                        {{ Form::open(array('route' => 'my-cars.store', 'class' => 'form-horizontal form-label-left')) }}
                    @endrole

                    @role('admin')
                        <div class="form-group{{ $errors->has('user[company]') ? ' has-error' : '' }}">
                            {{ Form::label('user[company]', 'Dealer', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12 form-required']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('user[company]', null , array('class' => 'form-control')) }}
                            </div>
                            <span class="help-block">
                                {{ $errors->first('user[company]') }}
                            </span>
                        </div>
                    @endrole

                    <div class="form-group{{ $errors->has('chassis_no') ? ' has-error' : '' }}">
                        {{ Form::label('chassis_no', 'Chassis Number', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12 form-required']) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::text('chassis_no', null , array('class' => 'form-control')) }}
                        </div>
                        <span class="help-block">
                            {{ $errors->first('chassis_no') }}
                        </span>
                    </div>

                    <div class="form-group{{ $errors->has('yom') ? ' has-error' : '' }}">
                        {{ Form::label('yom', 'Year of Manufacture', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12 form-required']) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::text('yom', null , array('class' => 'form-control')) }}
                        </div>
                        <span class="help-block">
                            {{ $errors->first('yom') }}
                        </span>
                    </div>

                    <div class="form-group{{ $errors->has('make') ? ' has-error' : '' }}">
                        {{ Form::label('make', 'Make', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12 form-required']) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::text('make', null , array('class' => 'form-control')) }}
                        </div>
                        <span class="help-block">
                            {{ $errors->first('make') }}
                        </span>
                    </div>

                    <div class="form-group{{ $errors->has('model') ? ' has-error' : '' }}">
                        {{ Form::label('model', 'Model', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12 form-required']) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::text('model', null , array('class' => 'form-control')) }}
                        </div>
                        <span class="help-block">
                            {{ $errors->first('model') }}
                        </span>
                    </div>

                    <div class="form-group{{ $errors->has('vac') ? ' has-error' : '' }}">
                        {{ Form::label('vac', 'VAC', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::text('vac', null , array('class' => 'form-control')) }}
                        </div>
                        <span class="help-block">
                            {{ $errors->first('vac') }}
                        </span>
                    </div>

                    <div class="form-group{{ $errors->has('omv') ? ' has-error' : '' }}">
                        {{ Form::label('omv', 'OMV', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::text('omv', null , array('class' => 'form-control')) }}
                        </div>
                        <span class="help-block">
                            {{ $errors->first('omv') }}
                        </span>
                    </div>

                    <div class="form-group{{ $errors->has('cevs') ? ' has-error' : '' }}">
                        {{ Form::label('cevs', 'eVES/VES', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::text('cevs', null , array('class' => 'form-control')) }}
                        </div>
                        <span class="help-block">
                            {{ $errors->first('cevs') }}
                        </span>
                    </div>

                    <div class="form-group{{ $errors->has('colour') ? ' has-error' : '' }}">
                        {{ Form::label('colour', 'Colour', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12 form-required']) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::text('colour', null , array('class' => 'form-control')) }}
                        </div>
                        <span class="help-block">
                            {{ $errors->first('colour') }}
                        </span>
                    </div>

                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                        {{ Form::label('price', 'Price', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12 form-required']) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::number('price', null , array('class' => 'form-control')) }}
                        </div>
                        <span class="help-block">
                            {{ $errors->first('price') }}
                        </span>
                    </div>

                    <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                        {{ Form::label('remarks', 'Remarks', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::textarea('remarks', null , array('class' => 'form-control')) }}
                        </div>
                        <span class="help-block">
                            {{ $errors->first('remarks') }}
                        </span>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <p class="form-required-text">Required Fields</p>
                            <button type="submit" class="btn btn-success">Add Car</button>
                        </div>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop