@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            @role('dealer')
            <h2>My Cars</h2>
            @endrole
            @role('admin')
            <h2>Manage Cars</h2>
            @endrole
        </div>
    </div>

    @if(session()->has('success'))
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            </div>
        </div>
    @endif

    @if(count($cars) > 0)
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="btn-group">
                    <button class="btn btn-success dropdown-toggle pull-right" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions <span class="fa fa-caret-down"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><button type="button" class="action-btn btn-link" id="delete">Delete</button></li>
                        <li><button type="button" class="action-btn btn-link" id="mark_as_sold">Mark as Sold</button></li>
                        <li><button type="button" class="action-btn btn-link" id="mark_as_reserved">Mark as Reserved</button></li>
                        <li><button type="button" class="action-btn btn-link" id="mark_as_none">Mark as None</button></li>
                        @role('admin')
                        <li><button type="button" class="action-btn btn-link" id="update_vac">Update VAC</button></li>
                        <li><button type="button" class="action-btn btn-link" id="update_price">Update Price</button></li>
                        <li><button type="button" class="action-btn btn-link" id="update_remarks">Update Remarks</button></li>
                        @endrole
                    </ul>
                </div>
                <a href="{{ url('/export/csv') }}" target="_blank">
                    <button class="btn btn-info" style="margin-bottom:0">
                        <i class="fa fa-download"></i> Export to Excel
                    </button>
                </a>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-bordered sc-datatable column-search">
                    <thead>
                    <tr>
                        <th class="sc-no-sort sc-no-search"><input class="check-all" type="checkbox"></th>
                        <th class="sc-no">No.</th>
                        @role('admin')
                        <th>Dealer</th>
                        @endrole
                        <th>Chassis No.</th>
                        <th>Unique No.</th>
                        <th>Year of Manufacture</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>VAC</th>
                        <th>OMV</th>
                        <th>eVES/VES</th>
                        <th>Colour</th>
                        <th>Dealer Price</th>
                        <th>Type</th>
                        <th>Remarks</th>
                        <th>Sold</th>
                        <th class="sc-u-date">U. Date</th>
                        <th class="sc-no-sort sc-no-search">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cars as $index => $car)
                        <tr>
                            <td class="text-center"><input type="checkbox" name="action[]" value="{{ $car->id }}" class="action-checkbox"></td>
                            <td>{{ $car->listing_no }}</td>
                            @role('admin')
                            <td>{{ $car->company }}</td>
                            @endrole
                            <td>{{ $car->chassis_no }}</td>
                            <td>{{ $car->id }}</td>
                            <td>{{ $car->yom }}</td>
                            <td>{{ $car->make }}</td>
                            <td>{{ $car->model }}</td>
                            <td>{{ $car->vac }}</td>
                            <td>{{ format_value('omv', number_format($car->omv)) }}</td>
                            <td>{{ $car->cevs }}</td>
                            <td>{{ $car->colour }}</td>
                            <td>{{ number_format($car->price) }}</td>
                            <td>{{ $car->type }}</td>
                            <td>{{ $car->remarks }}</td>
                            <td>{{ $car->sold }}</td>
                            <td>{{ date_format(date_create($car->updated_at), 'd/m/Y') }}</td>
                            <td>
                                @role('admin')
                                <a href="{{ url('/admin/cars/' . $car->id . '/edit') }}" class="btn btn-info">Edit</a>

                                {{ Form::open(array('url' => 'admin/cars/' . $car->id)) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <button type="button" class="btn btn-danger car-delete" id="car-{{ $car->id }}">Delete</button>
                                {{ Form::close() }}
                                @endrole

                                @role('dealer')
                                <a href="{{ url('/my-cars/' . $car->id . '/edit') }}" class="btn btn-info">Edit</a>
                                {{ Form::open(array('url' => 'my-cars/' . $car->id)) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <button type="button" class="btn btn-danger car-delete" id="car-{{ $car->id }}">Delete</button>
                                {{ Form::close() }}
                                @endrole
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <p>You don't have any cars yet.</p>
    @endif

    <script>
        (function ($) {
            $(function () {
                $('.car-delete').on('click', function (e) {
                    e.preventDefault();

                    var $form = $(this).parent('form');

                    bootbox.confirm({
                        message: 'Are you sure you want to delete this car?',
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                $form.submit();
                            }
                        }
                    });
                });
                $('.action-btn').on('click', function (e) {
                    e.preventDefault();
                    if($('.action-checkbox').is(':checked')) {
                        var $button = $(this);
                        var $table = $('.sc-datatable').dataTable();
                        var checkedCarsArray = [];
                        var url = "{{ url('cars/save-action') }}";
                        @role('admin')
                            url = "{{ url('admin/cars/save-action') }}";
                        @endrole
                        var callbackFunction = function (result) {
                            if (result) {
                                $('button').prop('disabled', true).css('opacity', 0.5);
                                $.ajax({
                                    url: url,
                                    type: 'POST',
                                    data: {
                                        '_token': '{{ csrf_token() }}',
                                        'actionType': $button.attr('id'),
                                        'updatedValue': result,
                                        'cars': checkedCarsArray
                                    }
                                }).done(function (response) {
                                    if (response.success) {
                                        $('button').prop('disabled', true).css('opacity', 1);
                                        location.reload();
                                    }
                                });
                            }
                        };
                        $('.action-checkbox:checked', $table.fnGetNodes()).each(function(){
                            checkedCarsArray.push($(this).val());
                        });
                        if ($button.text().toLowerCase().indexOf('update') > -1) {
                            var field = $button.attr('id').split("_").pop();

                            bootbox.prompt({
                                title: 'Please enter ' + field + ' below',
                                inputType: 'text',
                                callback: function(result) {
                                    callbackFunction(result);
                                }
                            });
                        } else {
                        bootbox.confirm({
                            message: 'Are you sure you want to ' + $button.text().toLowerCase() + ' the ' + checkedCarsArray.length + ' selected car(s)?',
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                    callbackFunction(result);
                                        }
                                    });
                                }
                    }
                    else {
                        bootbox.alert('Please select at least one car.')
                    }
                })
            });
        })(jQuery);
    </script>
@stop