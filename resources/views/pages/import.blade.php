@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h2>Add/Import Cars</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-danger" id="errors">
                <p>The following errors were found:</p>
                <ul></ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-success" id="success"></div>
        </div>
    </div>
    <div id="import_wrapper" class="row">
        <div class="col-xs-12">
            <button type="button" class="btn btn-success btn">Import Excel/CSV</button>
            <div class="x_panel" style="display: none;">
                <div class="x_content">
                    {{ Form::open(['id' => 'import_form', 'files' => 'true']) }}
                    {{ Form::file('csv') }}
                    {{--<input type="checkbox" id="clear_rows" name="clear_rows" value="1" checked/> <label for="clear_rows">Clear all rows</label>--}}
                    {{--<br>--}}
                    {{ Form::button('Read', array('type' => 'submit', 'class' => 'btn btn-success', 'disabled' => true)) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <br>
    <div id="cars_content_wrapper" class="row">
        <div class="col-xs-12">
            {{  Form::open(['id' => 'car_form', 'url' => 'save-csv']) }}
            <table id="cars_content_table" class="table table-bordered">
                <thead>
                <tr>
                    <th>No.</th>
                    @role('admin')
                    <th>Dealer</th>
                    @endrole
                    <th>Chassis No.</th>
                    <th>Year of Manufacture</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>VAC</th>
                    <th>OMV</th>
                    <th>eVES/VES</th>
                    <th>Colour</th>
                    <th>Dealer Price</th>
                    <th>Type</th>
                    <th>Remarks</th>
                    <th>Sold</th>
                    @role('admin')
                    <th>U. Date</th>
                    @endrole
                </tr>
                </thead>
                <tbody>
                <!-- Original Row to Clone -->
                <tr>
                    <td>1</td>
                    @role('admin')
                    <td><input class="field-dealer typeahead required" type="text" name="car[0][dealer]" value=""></td>
                    @endrole
                    <td><input class="field-chassis-no required" type="text" name="car[0][chassis_no]" value=""></td>
                    <td><input class="field-yom" type="text" name="car[0][yom]" value=""></td>
                    <td><input class="field-make typeahead required" type="text" name="car[0][make]" value=""></td>
                    <td><input class="field-model typeahead required" type="text" name="car[0][model]" value=""></td>
                    <td><input class="field-vac" type="text" name="car[0][vac]" value=""></td>
                    <td><input class="field-omv" type="text" name="car[0][omv]" value=""></td>
                    <td><input class="field-cevs" type="text" name="car[0][cevs]" value=""></td>
                    <td><input class="field-colour typeahead required" type="text" name="car[0][colour]" value=""></td>
                    <td><input class="field-price required" type="text" name="car[0][price]" value=""></td>
                    <td>
                        <select class="field-type" name="car[0][type]">
                            <option value="private">Private</option>
                            <option value="commercial">Commercial</option>
                        </select>
                    </td>
                    <td><textarea class="field-remarks" name="car[0][remarks]" rows="1"></textarea></td>
                    <td>
                        <select class="field-sold" name="car[0][sold]">
                            <option value="">Available</option>
                            <option value="sold">Sold</option>
                            <option value="reserved">Reserved</option>
                        </select>
                    </td>
                    @role('admin')
                    <td><input class="field-u-date" type="text" name="car[0][u_date]" value=""></td>
                    @endrole
                    <td style="width: 50px;">
                        <a href="#"><i class="add-row fa fa-plus-circle"></i></a>
                        <a href="#"><i class="delete-row fa fa-minus-circle"></i></a>
                    </td>
                </tr>
                <!-- -->
                </tbody>
            </table>
            <input type="checkbox" id="overwrite_all" name="overwrite_all" value="1"/> <label for="overwrite_all">Overwrite
                all existing cars on the database</label>
            {{ Form::button('Save', array('type' => 'submit', 'class' => 'btn btn-success pull-right')) }}
            {{ Form::close() }}
        </div>
    </div>

    <script>
        (function ($) {
            $(function () {
                var readButton = $('#import_wrapper button[type="submit"]', $importForm);
                var $importForm = $('form#import_form');
                var $carForm = $('form#car_form');
                var $originalRowClone = $('table tbody tr:first-child', $carForm).clone();
                var $deferred = $.Deferred();
                var dealerList = [];
                var makeList = [];
                var modelList = [];
                var colourList = [];

                // Dealers
                var $dealers = $.ajax({
                    url: '{{ route('ajax.get.dealers') }}',
                    dataType: 'json'
                }).done(function (data) {
                    dealerList = data;
                }); // TODO: Refactor this. -marc

                // Makes
                var $makes = $.ajax({
                    url: '{{ route('ajax.get.makes') }}',
                    dataType: 'json'
                }).done(function (data) {
                    makeList = data;
                }); // TODO: Refactor this. -marc

                // Models
                var $models = $.ajax({
                    url: '{{ route('ajax.get.models') }}',
                    dataType: 'json'
                }).done(function (data) {
                    modelList = data;
                }); // TODO: Refactor this. -marc

                // Colours
                var $colours = $.ajax({
                    url: '{{ route('ajax.get.colours') }}',
                    dataType: 'json'
                }).done(function (data) {
                    colourList = data;
                }); // TODO: Refactor this. -marc

                $('#import_wrapper').on('click', 'button', function () {
                    $(this).next().toggle();
                });

                //
                $.when($dealers).done(function () {
                    readButton.prop('disabled', false);

                    $importForm.on('submit', function (e) { // TODO: Refactor to use original row clone. -marc
                        e.preventDefault();

                        readButton.prop('disabled', true).append(' <i class="fa fa-spinner fa-spin"></i>').css('opacity', 0.5);

                        url = "{{ url('import') }}";

                        @role('admin')
                        url = "{{ url('admin/import') }}";
                        @endrole

                        //
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: new FormData($importForm[0]),
                            contentType: false,
                            processData: false
                        }).done(function (response) {
                            $('#errors').hide();
                            $('#success').hide();
                            $('button[type="submit"] .fa-spinner', $importForm).remove();
                            $('button[type="submit"]', $importForm).css('opacity', 1);

                            if (response.success) {
                                //
//                            if ($('#clear_rows', $importForm).is(':checked')) {
                                $('#cars_content_table tbody').empty();
//                            }

                                var date = new Date();

                                $.each(response.data, function (index, value) {
                                    var systemChangedFields = [];
                                    @role('admin')
                                    var dealer = value.dealer || '';
                                    @endrole
                                    var chassisNo = value.chassis_no || '';
                                    var yom = value.yom || '';
                                    var make = value.make || '';
                                    var model = value.model || '';
                                    var vac = value.vac || '';
                                    var omv = parseFloat(value.omv) || '';
                                    var cevs = parseInt(value.ves) || '';
                                    var colour = value.colour || '-';
                                    var price = value.price || '';
                                    var type = value.type || '';
                                    var remarks = value.remarks || '';
                                    var sold = value.sold || '';

                                    @role('admin')
                                    var uDate = value.u_date || '';

                                    if ($.inArrayIn(dealer, dealerList) == -1) {
                                        dealer = '';
                                    }

                                    // Update Date
                                    if (uDate.hasOwnProperty('date')) {
                                        uDate = uDate.date.substr(0, 10);

                                        // Workaround for dd/mm/yyyy. TODO: Fix this. -marc
                                        var tempDate = uDate.split('-');
                                        uDate = tempDate[2] + '/' + tempDate[1] + '/' + tempDate[0];
                                    }
                                    @endrole

                                    // System generated chassis number
                                    if (!chassisNo || chassisNo.toString().toLowerCase() == 'pending') {
                                        chassisNo = 'SYSGEN-' + date.yymmdd() + '-' + randomString(8).toUpperCase();
                                        systemChangedFields.push('chassis_no');
                                    }

                                    // Price with value of sold
                                    if (!parseFloat(price)) { // TODO:
                                        if (price.toString().toLowerCase() == 'sold') {
                                            sold = 'sold';
                                            systemChangedFields.push('sold');
                                        } else if (price.toString().toLowerCase() == 'reserved') {
                                            sold = 'reserved';
                                            systemChangedFields.push('sold');
                                        }

                                        price = '-';
                                        systemChangedFields.push('price');
                                    }

                                    // Split make + model
                                    var modelSplit = model.split(' ');

                                    if (!make) {
                                        var first = modelSplit[0];
                                        var firstSecond = modelSplit[0] + ' ' + modelSplit[1];

                                        if ($.inArrayIn(first, makeList) !== -1) { // For one word
                                            make = first;
                                            model = model.replace(first, '').trim();
                                        } else if ($.inArrayIn(firstSecond, makeList) !== -1) { // For two words
                                            make = firstSecond;
                                            model = model.replace(firstSecond, '').trim();
                                        }
                                    }

                                    // Fix Common Make Typo
                                    // var makeMapObj = {
                                    //     'Mercedes' : 'Mercedes-Benz',
                                    //     'Benz' : 'Mercedes-Benz',
                                    //     'Mercedes benz' : 'Mercedes-Benz',
                                    //     'Merdeces benz' : 'Mercedes-Benz',
                                    //     'Mercedez-Benz' : 'Mercedes-Benz',
                                    //     'Toyta' : 'Toyota'
                                    // };
                                    // make = fixTypo(make, makeMapObj);

                                    make = fixMakeTypo(make);

                                    // Fix Common Model Typo
                                    var modelMapObj = {
                                        'A200' : 'A 200',
                                        'B200' : 'B 200',
                                        'C200' : 'C 200',
                                        'CLA180' : 'CLA 180',
                                        'CLA200' : 'CLA 200',
                                        'GLA180' : 'GLA 180',
                                        'GLA200' : 'GLA 200',
                                        'GLB200' : 'GLB 200',
                                        'GLC200' : 'GLC 200',
                                        'GLC250' : 'GLC 250',
                                        'GLC300' : 'GLC 300',
                                        'GLC43' : 'GLC 43',
                                        '1.3A' : '1.3 A',
                                        '1.3GF' : '1.3 GF',
                                        '1.5A' : '1.5 A',
                                        '1.5G' : '1.5 G',
                                        '1.5LX' : '1.5 LX',
                                        '1.5X' : '1.5 X',
                                        '1.8V' : '1.8 V',
                                        '1.8X' : '1.8 X',
                                        '2.0X' : '2.0 X',
                                        '2.5X' : '2.5 X',
                                        '2.5Z' : '2.5 Z',
                                        '2.5ZG' : '2.5 ZG'
                                    };

                                    model = fixTypo(model, modelMapObj);

                                    var html = '<tr>' +
                                        '<td></td>' +

                                    @role('admin')
                                    '<td>' +
                                    '<input class="field-dealer typeahead required" type="text" name="car[' + index + '][dealer]" value="' + dealer + '">' +
                                    '</td>' +
                                    @endrole

                                    '<td>' +
                                    '<input class="field-chassis-no required" type="text" name="car[' + index + '][chassis_no]" value="' + chassisNo + '">' +
                                    '</td>' +
                                    '<td>' +
                                    '<input class="field-yom" type="text" name="car[' + index + '][yom]" value="' + yom + '">' +
                                    '</td>' +
                                    '<td>' +
                                    '<input class="field-make typeahead required" type="text" name="car[' + index + '][make]" value="' + make + '">' +
                                    '</td>' +
                                    '<td>' +
                                    '<input class="field-model typeahead required" type="text" name="car[' + index + '][model]" value="' + model + '">' +
                                    '</td>' +
                                    '<td>' +
                                    '<input class="field-vac" type="text" name="car[' + index + '][vac]" value="' + vac + '">' +
                                    '</td>' +
                                    '<td>' +
                                    '<input class="field-omv" type="text" name="car[' + index + '][omv]" value="' + omv + '">' +
                                    '</td>' +
                                    '<td>' +
                                    '<input class="field-cevs" type="text" name="car[' + index + '][cevs]" value="' + cevs + '">' +
                                    '</td>' +
                                    '<td>' +
                                    '<input class="field-colour typeahead required" type="text" name="car[' + index + '][colour]" value="' + colour + '">' +
                                    '</td>' +
                                    '<td>' +
                                    '<input class="field-price required" type="text" name="car[' + index + '][price]" value="' + price + '">' +
                                    '</td>' +
                                    '<td>' +
                                    '<select class="field-type" name="car[' + index + '][type]">' +
                                    '<option value="private">Private</option>' +
                                    '<option value="commercial">Commercial</option>' +
                                    '</td>' +
                                    '<td>' +
                                    '<textarea class="field-remarks" name="car[' + index + '][remarks]" rows="1">' + remarks + '</textarea>' +
                                    '</td>' +
                                    '<td>' +
                                    '<select class="field-sold" name="car[' + index + '][sold]">' +
                                    '<option value="">Available</option>' +
                                    '<option value="sold">Sold</option>' +
                                    '<option value="reserved">Reserved</option>' +
                                    '</select>' +
                                    '</td>' +

                                    @role('admin')
                                    '<td>' +
                                    '<input class="field-u-date" type="text" name="car[' + index + '][u_date]" value="' + uDate + '">' +
                                    '</td>' +
                                    @endrole

                                    '<td style="width: 50px;">' +
                                    '<a href="#"><i class="add-row fa fa-plus-circle"></i></a> ' +
                                    '<a href="#"><i class="delete-row fa fa-minus-circle"></i></a>' +
                                    '</td>' +
                                    '</tr>';

                                    $html = $(html).appendTo($('#cars_content_table tbody'));

                                    // Set Type Value
                                    if (type.toLowerCase() == 'commercial' || type.toLowerCase() == 'com') {
                                        type = 'commercial';
                                    } else {
                                        type = 'private';
                                    }

                                    $html.find('.field-type').val(type);

                                    // Set Sold Value
                                    if (sold.toLowerCase() == 'y' || sold.toLowerCase() == 'yes' || sold.toLowerCase() == 'sold') {
                                        sold = 'sold';
                                    } else if (sold.toLowerCase() == 'reserved' || sold.toLowerCase() == 'reserve' || sold.toLowerCase() == 'res') {
                                        sold = 'reserved';
                                    } else if (sold.toLowerCase() == 'available' || sold.toLowerCase() == '') {
                                        sold = '';
                                    }

                                    $html.find('.field-sold').val(sold);

                                    // Add class to system generated field values
                                    if (systemChangedFields.length > 0) {
                                        $.each(systemChangedFields, function (i, v) {
                                            $('#cars_content_table tr:eq(' + (index + 1) + ') input[name^=car]').each(function () {
                                                nameAttribute = $(this).attr('name');
                                                if (nameAttribute.indexOf(v) > -1) {
                                                    $(this).addClass('system-gen');
                                                }
                                            });
                                        });
                                    }
                                });

                                initializeTypeaheads();
                                resetRowAndIndexes();
                                formatTable();
                            } else {
                                $('#errors ul').empty();

                                $.each(response.data, function (index, value) {
                                    $('#errors ul').append('<li>' + value + '</li>');
                                });

                                $('#errors').show();
                            }
                        }).always(function () {
                            readButton.prop('disabled', false);
                        });
                    });

                    $('body').on('blur focusout', '.field-dealer', function () {
                        var $this = $(this);
                        var $td = $this.closest('td');

                        if ($.inArray($this.val(), dealerList) == -1) {
                            $this.val('');
                            $td.addClass('border-error');
                        } else {
                            $td.removeClass('border-error');
                        }
                    });
                });

                //
                $carForm.on('click', '.add-row', function (e) {
                    e.preventDefault();

                    var $newRow = $originalRowClone.clone();

                    $('input, select, textarea', $newRow).each(function () {
                        $(this).attr('name', $(this).attr('name').replace(/\d+/, getMaxIndex() + 1));
                    });

                    $newRow.insertAfter($(this).closest('tr'));
                    resetRowAndIndexes();
                    initializeTypeaheads();
                });

                //
                $carForm.on('click', '.delete-row', function (e) {
                    e.preventDefault();

                    var count = $('tbody tr', $carForm).length;

                    if (count > 1) {
                        $(this).closest('tr').remove();
                        resetRowAndIndexes();
                    } else {
                        alert('You should have at least one car.');
                    }
                });

                //
                $carForm.on('submit', function (e) {
                    e.preventDefault();

                    if ($('[name="overwrite_all"]', $carForm).is(':checked')) {
                        bootbox.confirm({
                            size: 'medium',
                            message: 'This operation will overwrite all cars. Are you sure you want to continue?',
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                checkOverwrite(result, $(this));
                            }
                        });
                    } else {
                        saveRecords()
                    }
                });

                //
                function checkOverwrite(result, modal) {
                    if (result === true) {
                        bootbox.confirm({
                            size: 'medium',
                            message: 'Do you want to backup/export your current car listing?',
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                if (result === true) {
                                    window.open('<?php echo url("/export/csv") ?>', '_blank');
                                } else {
                                    modal.modal('hide');
                                }

                                saveRecords();
                            }
                        });
                    } else {
                        modal.modal('hide');
                    }
                }

                //
                function saveRecords() {
                    formatTable();

                    $('#errors ul').empty();
                    var $saveButton = $('button[type="submit"]', $carForm);

                    $saveButton.prop('disabled', true).append(' <i class="fa fa-spinner fa-spin"></i>').css('opacity', 0.5);

                    var url = "{{ url('import/save-csv') }}";

                    @role('admin')
                        url = "{{ url('admin/import/save-csv') }}";
                    @endrole

                    var carsArray = [];
                    var dealerChassisNo = [];
                    var duplicates = [];

                    $('#cars_content_table tbody tr').each(function () {
                        var carsObj = {};
                        $(this).find('td').each(function (key, value) {
                            carsObj[key] = $(this).find('input').val();
                        });
                        carsArray.push(carsObj);
                    });

                    $.each(carsArray, function (key, value) {
                        @role('admin')
                            var valueToCheck = value[1] + value[2]; //dealer + chassis_no
                        @endrole

                        @role('dealer')
                            var valueToCheck = value[1]; //chassis_no
                        @endrole
                        if ($.inArrayIn(valueToCheck, dealerChassisNo) !== -1 && valueToCheck !== '') {
                            duplicates.push(key);

                            @role('admin')
                                $('#cars_content_table tbody tr td input[value="' + value[1] + '"]').each(function () {
                                    if ($(this).closest('tr').find('td:eq(2) input').val() == value[2]) {
                                        $(this).closest('tr').find('td:eq(2)').addClass('border-error');
                                    }
                                });
                            @endrole
                            @role('dealer')
                                $('#cars_content_table tbody tr td').each(function () {
                                    if ($(this).closest('tr').find('td:eq(1) input').val() == value[1]) {
                                        $(this).closest('tr').find('td:eq(1)').addClass('border-error');
                                    }
                                });
                            @endrole
                        }

                        dealerChassisNo.push(valueToCheck);
                    });

                    if (duplicates.length > 0) {
                        $('#car_form button[type="submit"] .fa-spinner').remove();
                        $('#car_form button[type="submit"]').css('opacity', 1);
                        $saveButton.prop('disabled', false);

                        $('#errors ul').append('<li>' + 'Duplicate chassis no. found. Please enter a unique chassis no. or leave as blank to get our system to assign your vehicle a system generated chassis no. ' + '</li>');
                        $('#errors').fadeIn(500).delay(3000).fadeOut(500);
                        focusError();
                    }
                    else {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: $carForm.serialize()
                    }).done(function (response) {
                        $('#errors').hide();
                        $('#success').empty();
                        $('#car_form button[type="submit"] .fa-spinner').remove();
                        $('#car_form button[type="submit"]').css('opacity', 1);

                        if (response.success) {
                            $('#success').append('<p>Import successful:</p>');
                            $('#success').append('<ul>');

                            if (response.data.recordUpdateCount == 1) {
                                $('#success').append('<li>' + response.data.recordUpdateCount + ' car updated.' + '</li>');
                            } else if (response.data.recordUpdateCount > 1) {
                                $('#success').append('<li>' + response.data.recordUpdateCount + ' cars updated.' + '</li>');
                            }

                            if (response.data.recordSaveCount == 1) {
                                $('#success').append('<li>' + response.data.recordSaveCount + ' car created.' + '</li>');
                            } else if (response.data.recordSaveCount > 1) {
                                $('#success').append('<li>' + response.data.recordSaveCount + ' cars created.' + '</li>');
                            }

                            $('#success').append('</ul>');

                            $('#success').show();
                            $('#errors').hide();

                            $('#import_wrapper').hide();
                            $('#cars_content_wrapper').hide();
                            $('#cars_content_table tbody').empty();
                        } else {
                            $.each(response.data, function (index, value) {
                                $('#errors ul').append('<li>' + value + '</li>');
                            });

                                $('#errors').fadeIn(500).delay(3000).fadeOut(500);
                        }
                    }).fail(function (data) {
                        $('#car_form button[type="submit"] .fa-spinner').remove();
                        $('#car_form button[type="submit"]').css('opacity', 1);
                        $('#errors ul').empty();

                        var errors = $.parseJSON(data.responseText);

                        $.each(errors, function (index, rowErrors) {
                            $.each(rowErrors, function (key, text) {
                                $('#errors ul').append('<li>' + text + '</li>');
                            });
                        });

                            $('#errors').fadeIn(500).delay(3000).fadeOut(500);
                    }).always(function () {
                        $saveButton.prop('disabled', false);
                        })
                    };
                }
                //
                function focusError() {
                    var container = $('html,body');
                    var scrollTo = $('.border-error:first');
                    container.animate({scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop(),
                                        scrollLeft: 0}, 300);
                    $('.border-error:first input').focus()
                                                  .val($('.border-error:first input').val()); //move caret to last character
                }

                //
                function getMaxIndex() {
                    var max = 0;

                    $('input.chassis-number', $carForm).each(function () {
                        var index = parseInt($(this).attr('name').match(/\d+/));

                        if (index > max) {
                            max = index;
                        }
                    });

                    return max;
                }

                //
                function resetRowAndIndexes() {
                    var index = 0;
                    var $trs = $('tbody tr', $carForm);

                    $trs.each(function () {
                        // Number Column
                        $('td:first-child', $(this)).text(index + 1);

                        // Form Fields
                        $('input, select, textarea', $(this)).each(function () {
                            $(this).attr('name', $(this).attr('name').replace(/\d+/, index));
                        });

                        index++;
                    });
                }

                //
                function initializeTypeaheads() {
                    $.when($dealers, $makes, $models, $colours).then(function () {
                        $('.field-dealer.typeahead').typeahead({
                            source: dealerList
                        }); // TODO: Refactor this. -marc

                        $('.field-make.typeahead').typeahead({
                            source: makeList
                        }); // TODO: Refactor this. -marc

                        $('.field-model.typeahead').typeahead({
                            source: modelList
                        }); // TODO: Refactor this. -marc

                        $('.field-colour.typeahead').typeahead({
                            source: colourList
                        }); // TODO: Refactor this. -marc
                    });
                }

                //
                function fixMakeTypo(make) {
                    // Mercedes-Benz
                    if ($.inArrayIn(make, ['Mercedes', 'Benz', 'Mercedes benz', 'Merdeces benz', 'Mercedez-Benz']) !== -1) {
                        return 'Mercedes-Benz'
                    }

                    // Toyota
                    if ($.inArrayIn(make, ['Toyta']) !== -1) {
                        return 'Toyota'
                    }

                    return make;
                }

                //
                function formatTable() {
                    $('input[type="text"]', $carForm).each(function () {
                        var $this = $(this);

                        if ($this.val() === '' && $this.hasClass('required')) {
                            $this.parent().addClass('border-error');
                        } else {
                            $this.parent().removeClass('border-error');
                        }
                    })
                }

                //
                function fixTypo(str, mapObj){
                    var regex = new RegExp(Object.keys(mapObj).join("|"),"gi");

                    return str.replace(regex, function(matched){
                        return mapObj[matched];
                    });
                }
            });
        })(jQuery)
    </script>
@stop