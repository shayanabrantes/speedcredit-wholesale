@extends('layouts.default')

@section('content')
    <style type="text/css" media="print">
        body {
            visibility: hidden;
            display: none
        }
    </style>
    <div class="row">
        <div class="col-xs-12">
            <h2>Public Car Listing (Jesse's Version) - Multi Test</h2>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered sc-datatable-select2 column-search public-listing">
                <thead>
                <tr class="secondary">
                    <th class="sc-no">No.</th>
                    <th>Dealer</th>
                    <th>Chassis No.</th>
                    <th>Unique No.</th>
                    <th>YOM</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>VAC</th>
                    <th>OMV</th>
                    <th>Colour</th>
                    <th>Dealer Price</th>
                    <th>Wholesale Price</th>
                    <th>Type</th>
                    <th>Remarks</th>
                    <th>Sold</th>
                    <th class="sc-u-date">U. Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cars as $index => $car)
                    @php
                        $yom_class = '';
                        $yom = $car->yom;
                        $cur_year = date('Y');

                        if ($yom) {
                            if ($cur_year <= $yom) {
                                $yom_class = 'current';
                            } else if ($cur_year - $yom >= 2) {
                                $yom_class = 'old';
                            }
                        }

                        if (!empty((float) $car->price)) {
                            $dealer_price = number_format($car->price);
                            $wholesale_price = number_format(getWholesalePrice($car->price));
                        } else {
                            $dealer_price = '';
                            $wholesale_price = '';
                        }
                    @endphp
                    <tr>
                        <td class="listing-no">{{ $car->listing_no }}</td>
                        <td class="dealer">{{ $car->company }}</td>
                        <td class="chassis-no">{{ $car->chassis_no }}</td>
                        <td class="unique-no">{{ $car->id }}</td>
                        <td class="{{ $yom_class }}">{{ $car->yom }}</td>
                        <td class="make">{{ $car->make }}</td>
                        <td class="model">{{ $car->model }}</td>
                        <td class="vac">{{ substr($car->vac, 0, 1) }}</td>
                        <td class="omv" style="text-align: right;">{{ format_value('omv', number_format($car->omv)) }}</td>
                        <td class="colour">{{ $car->colour }}</td>
                        <td class="dealer-price" style="text-align: right;"><strong>{{ $dealer_price }}</strong></td>
                        <td class="wholesale-price" style="text-align: right;"><strong>{{ $wholesale_price }}</strong></td>
                        <td class="type">{{ format_value('type', $car->type) }}</td>
                        <td class="remarks">{{ $car->remarks }}</td>
                        <td @if(!empty($car->sold)) {!! 'class="sold"' !!} @endif>{{ $car->sold }}</td>
                        <td class="updated-at">{{ date_format(date_create($car->updated_at), 'd/m/Y') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        (function ($){
            $(function () {
                //
                var clipboard = new Clipboard('.public-listing tbody tr td:first-child', {
                    text: function(e) {
                        return getRowText(e);
                    }
                });

                clipboard.on('success', function() {
                    if (document.execCommand('copy')) {
                        var successHtml = "<div class='alert alert-success' id='success'>" +
                            "<p>Text copied to clipboard.</p></div>";

                        $('.public-listing').prepend(successHtml);
                        $('#success').fadeIn(500).delay(3000).fadeOut(500);
                    }
                });

                //
                $('.public-listing tbody tr td:first-child').on('click', function () {
                    if (!document.execCommand('copy')) {
                        window.alert(getRowText(this));
                    }
                });

                //
                function getRowText(e) {
                    var rowObject = $(e).closest('tr');

                    return rowObject.find('td.unique-no').text().toUpperCase() + ' ' +
                           rowObject.find('td.dealer').text().toUpperCase() + ' ' +
                           rowObject.find('td.chassis-no').text().toUpperCase() + ' ' +
                           rowObject.find('td.make').text().toUpperCase() + ' ' +
                           rowObject.find('td.model').text().toUpperCase() + ' ' +
                           rowObject.find('td.colour').text().toUpperCase() + ' ' +
                           rowObject.find('td.dealer-price').text().toUpperCase();
                }
            });
        })(jQuery);
    </script>
@stop