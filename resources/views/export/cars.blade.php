<html>
<head>
    <title>{{ $title }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        .container {
            margin: 0 auto;
        }

        .public-listing {
            border-collapse: collapse;
            font-family: 'Arial', sans-serif !important;
            font-size: 10px;
            width: 100%;
        }

        .public-listing thead th {
            background: none;
            color: #000;
        }

        .public-listing thead tr.primary {
            font-size: 16px;
        }

        .public-listing thead tr.secondary {
            text-decoration: underline;
        }

        .public-listing tbody tr:nth-child(even) {
            background: #eeeeee;
        }

        .public-listing tbody tr:nth-child(odd) {
            background: #ffffff;
        }

        .public-listing tbody td {
            text-transform: uppercase;
        }

        .public-listing tbody td.remarks {
            text-transform: none;
        }

        .public-listing tbody td.current {
            background-color: #00bfff !important;
            color: #000;
        }

        .public-listing tbody td.old {
            background-color: #ff0000 !important;
            color: #fff;
        }

        .public-listing tbody td.sold {
            background-color: #ffff00 !important;
            color: #000;
        }

        .public-listing th,
        .public-listing td {
            border: 1px solid #ddd;
            padding: 2px 5px;
        }

        @media print {
            .public-listing tr td,
            .public-listing tr th {
                font-size: 6pt;
            }

            .public-listing tr,
            .public-listing th,
            .public-listing td {
                border: 1px solid #ddd;
                padding: 2px;
                height: 14px;
            }

            .public-listing td:nth-child(3) {
                max-width: 80px;
            }

            .public-listing td:nth-child(4) {
                max-width: 100px;
            }

            .public-listing td:nth-child(10) {
                max-width: 100px;
            }
        }
    </style>
</head>
<body>
<div id="main" class="container">
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered public-listing">
                <thead>
                <tr class="primary">
                    <th colspan="14" style="text-align: center">
                        SPEED CREDIT PTE LTD - GLEN YEO (9619 7164) wholesale@speedcredit.com.sg<br/>
                        For a faster transaction, get the latest list of cars here: <a href="https://www.speedcredit.com.sg/wholesale/" target="_blank">https://www.speedcredit.com.sg/wholesale/</a> and quote the unique number of your car choice. <br/>
                        Wholesales PDF Date of Publish: <?php echo date('d/m/Y') ?>
                    </th>
                </tr>
                <tr class="secondary">
                    <th>No.</th>
                    <th>Unique No.</th>
                    <th>YOM</th>
                    <th>Make</th>
                    <th style="width: 30%">Model</th>
                    <th>VAC</th>
                    <th>OMV</th>
                    <th>eVES/VES</th>
                    <th>Colour</th>
                    <th>Wholesale Price</th>
                    <th>Type</th>
                    <th>Remarks</th>
                    <th>Sold</th>
                    <th>U. Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cars as $index => $car)
                    @php
                        $yom_class = '';
                        $yom = $car->yom;
                        $cur_year = date('Y');

                        if ($yom) {
                            if ($cur_year <= $yom) {
                                $yom_class = 'current';
                            } else if ($cur_year - $yom >= 2) {
                                $yom_class = 'old';
                            }
                        }

                        if (!empty((float) $car->price)) {
                            $wholesale_price = number_format(getWholesalePrice($car->price));
                        } else {
                            $wholesale_price = '-';
                        }
                    @endphp
                    <tr>
                        <td>{{ $car->listing_no }}</td>
                        <td>{{ $car->id }}</td>
                        <td class="{{ $yom_class }}">{{ $car->yom }}</td>
                        <td>{{ $car->make }}</td>
                        <td>{{ $car->model }}</td>
                        <td>{{ substr($car->vac, 0, 1) }}</td>
                        <td style="text-align: right;">{{ format_value('omv', number_format($car->omv)) }}</td>
                        <td>{{ $car->cevs }}</td>
                        <td>{{ $car->colour }}</td>
                        <td style="text-align: right;"><strong>{{ $wholesale_price }}</strong></td>
                        <td>{{ format_value('type', $car->type) }}</td>
                        <td class="remarks">{{ $car->remarks }}</td>
                        <td @if(!empty($car->sold)) {!! 'class="sold"' !!} @endif>{{ $car->sold }}</td>
                        <td>{{ date_format(date_create($car->updated_at), 'd/m/Y') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>