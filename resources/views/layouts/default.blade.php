<!DOCTYPE html>
<html>
<head>
    <title>Speed Credit Wholesale List</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link href="{{ URL::asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- NProgress -->
    <link href="{{ URL::asset('vendors/nprogress/nprogress.css') }}" rel="stylesheet">

    <!-- DataTables -->
    <link href="{{ URL::asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Others -->
    <script src="//cdn.jsdelivr.net/npm/clipboard@1/dist/clipboard.min.js"></script>
    <!-- <link href="{{ URL::asset('css/select2.min.css') }}" rel="stylesheet"> -->

    <!-- Custom Theme Style -->
    <link href="{{ URL::asset('build/css/custom.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/style.css?v=170728') }}" rel="stylesheet">
</head>
<body class="<?php if(Auth::user()) echo 'nav-md logged-in'; ?>">
@if(Auth::user())
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="{{ url('/') }}" class="site_title">Speed Credit Wholesale List</a>
                    </div>

                    <div class="profile"><!--img_2 -->
                        {{--<div class="profile_pic">--}}
                            {{--<img src="https://dummyimage.com/150x150/d6d6d6/fff" alt="..." class="img-circle profile_img">--}}
                        {{--</div>--}}
                        <div class="profile_info">
                            <span>Hi,</span>
                            <h2>{{ Auth::user()->name }}!</h2>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            {{--<h3>General</h3>--}}

                            <ul class="nav side-menu">
                                <li>
                                    <a href="{{ url('/car-listing') }}">
                                        <i class="fa fa-list"></i> Public Car Listing
                                    </a>
                                </li>
                                @if (Auth::user()->roles->first()->name == 'dealer')
                                    {{--<li>--}}
                                        {{--<a href="{{ url('/dashboard') }}">--}}
                                            {{--<i class="fa fa-tachometer"></i> Dashboard--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    <li><a href="{{ url('/my-cars') }}">My Cars</a></li>
                                    {{--<li><a href="{{ url('/import') }}">Add/Import Cars</a></li>--}}
                                    {{--<li><a href="{{ url('/my-cars/create') }}">Add a Car</a></li>--}}
                                @elseif (Auth::user()->roles->first()->name == 'admin')
                                    {{--<li>--}}
                                        {{--<a href="{{ url('/admin/dashboard') }}">--}}
                                            {{--<i class="fa fa-tachometer"></i> Dashboard--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-car"></i> Cars <span class="fa fa-chevron-down"></span>
                                        </a>
                                        <ul class="nav child_menu" role="menu">
                                            {{--<li><a href="{{ url('/admin/cars/create') }}">Add Car</a></li>--}}
                                            <li><a href="{{ url('/admin/import') }}">Add/Import Cars</a></li>
                                            <li><a href="{{ url('/admin/cars') }}">Manage Cars</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users"></i> Dealers <span class="fa fa-chevron-down"></span>
                                        </a>
                                        <ul class="nav child_menu" role="menu">
                                            <li><a href="{{ url('/admin/dealers/create') }}">Add Dealer</a></li>
                                            <li><a href="{{ url('/admin/dealers') }}">Manage Dealers</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/admin/send-email') }}">
                                            <i class="fa fa-envelope"></i> Send Mass Email
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>

                    {{--<div class="sidebar-footer hidden-small">--}}
                    {{----}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    {{--<img src="https://dummyimage.com/150x150/d6d6d6/fff" alt="{{ Auth::user()->name }}">--}}
                                    {{ Auth::user()->name }}
                                    <span class="fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    @if (Auth::user()->roles->first()->name == 'dealer')
                                        <li><a href="{{ url('/profile') }}"> Profile</a></li>
                                    @elseif (Auth::user()->roles->first()->name == 'admin')
                                        <li><a href="{{ url('/admin/profile') }}"> Profile</a></li>
                                    @endif
                                    {{--<li>--}}
                                    {{--<a href="javascript:;">--}}
                                    {{--<i class="fa fa-cog pull-right"></i>--}}
                                    {{--<span>Settings</span>--}}
                                    {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                    {{--<a href="javascript:;">--}}
                                    {{--<i class="fa fa-info-circle pull-right"></i>--}}
                                    {{--<span>Help</span>--}}
                                    {{--</a>--}}
                                    {{--</li>--}}
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
                            {{--<li role="presentation" class="dropdown">--}}
                            {{--<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">--}}
                            {{--<i class="fa fa-envelope-o"></i>--}}
                            {{--<span class="badge bg-green">6</span>--}}
                            {{--</a>--}}
                            {{--<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">--}}
                            {{--<li>--}}
                            {{--<a>--}}
                            {{--<span class="image"><img src="images/img.jpg" alt="Profile Image"></span>--}}
                            {{--<span>--}}
                            {{--<span>John Smith</span>--}}
                            {{--<span class="time">3 mins ago</span>--}}
                            {{--</span>--}}
                            {{--<span class="message">--}}
                            {{--Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                            {{--</span>--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                            {{--<a>--}}
                            {{--<span class="image"><img src="images/img.jpg" alt="Profile Image"></span>--}}
                            {{--<span>--}}
                            {{--<span>John Smith</span>--}}
                            {{--<span class="time">3 mins ago</span>--}}
                            {{--</span>--}}
                            {{--<span class="message">--}}
                            {{--Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                            {{--</span>--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                            {{--<a>--}}
                            {{--<span class="image"><img src="images/img.jpg" alt="Profile Image"></span>--}}
                            {{--<span>--}}
                            {{--<span>John Smith</span>--}}
                            {{--<span class="time">3 mins ago</span>--}}
                            {{--</span>--}}
                            {{--<span class="message">--}}
                            {{--Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                            {{--</span>--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                            {{--<a>--}}
                            {{--<span class="image"><img src="images/img.jpg" alt="Profile Image"></span>--}}
                            {{--<span>--}}
                            {{--<span>John Smith</span>--}}
                            {{--<span class="time">3 mins ago</span>--}}
                            {{--</span>--}}
                            {{--<span class="message">--}}
                            {{--Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                            {{--</span>--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                            {{--<div class="text-center">--}}
                            {{--<a>--}}
                            {{--<strong>See All Alerts</strong>--}}
                            {{--<i class="fa fa-angle-right"></i>--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--</li>--}}
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="right_col" role="main">
                @yield('content')
            </div>
            <footer>
                <div class="copyright text-right">© Copyright 2017 Speed Credit. All rights reserved.</div>
            </footer>
        </div>
    </div>
@else
    <div class="default-navbar">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#default-nav" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">Speed Credit Wholesale List</a>
            </div>
            <div class="collapse navbar-collapse" id="default-nav">
                <ul class="nav navbar-nav navbar-right">
                    {{--<li><a href="{{ url('register') }}">Register</a></li>--}}
                    <li><a href="{{ url('login') }}">Log In</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <br/>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                @yield('content')
            </div>
        </div>
    </div>
@endif
<!-- Bootstrap -->
<script src="{{ URL::asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<!-- FastClick -->
<script src="{{ URL::asset('vendors/fastclick/lib/fastclick.js') }}"></script>

<!-- NProgress -->
<script src="{{ URL::asset('vendors/nprogress/nprogress.js') }}"></script>

<!-- Typeahead -->
<script src="{{ URL::asset('js/bootstrap3-typeahead.min.js') }}"></script>

<!-- Others -->
<script src="{{ URL::asset('js/moment.min.js') }}"></script>
<script src="{{ URL::asset('js/bootbox.min.js') }}"></script>
<!-- <script src="{{ URL::asset('js/select2.min.js') }}"></script> -->

<!-- Datatables -->
<script src="{{ URL::asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ URL::asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ URL::asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ URL::asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ URL::asset('vendors/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ URL::asset('vendors/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('vendors/pdfmake/build/vfs_fonts.js') }}"></script>

<!-- TinyMCE -->
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=oaxjt9v6vu7f2orfbo00xnxn1myuya7tsw507r5lwri17pv8"></script>

<!-- Custom Theme Scripts -->
<script src="{{ URL::asset('build/js/custom.min.js') }}"></script>
<script src="{{ URL::asset('js/script.js?v=170728') }}"></script>
</body>
</html>