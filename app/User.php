<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company',
        'name',
        'email',
        'password',
        'tel_no',
        'mobile_no',
        'fax',
        'business_reg_no',
        'address1',
        'address2',
        'postal_code',
        'email_frequency'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];


    public function cars()
    {
        return $this->hasMany('App\Car');
    }

    public function uploads()
    {
        return $this->hasMany('App\Upload');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

}