<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Mail;

class SendCarListingEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'carlistingemail:send {frequency}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send car listing to users depending on their settings (weekly or monthly)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $frequency = $this->argument('frequency');
        $users = User::where('email_frequency', $frequency)->get();

        foreach ($users as $user) {
            Mail::send('emails.car_listing', ['user' => $user], function ($m) use ($user) {
                $m->from('jesse.tan@speedcredit.com.sg', 'Speed Credit Wholesale');

                $m->to($user->email, $user->name)->subject('Speed Credit Wholesale - Car Listing');
            });
        }
    }
}
