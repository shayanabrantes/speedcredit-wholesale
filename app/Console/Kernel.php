<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         Commands\SendCarListingEmail::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('carlistingemail:send minute')->everyMinute(); //for testing only - shayan
        $schedule->command('carlistingemail:send daily')->daily(); //for testing only - shayan
        $schedule->command('carlistingemail:send weekly')->weekly();
//        $schedule->command('carlistingemail:send monthly')->monthly();
    }
}