<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{

    protected $fillable = [
        'mail_default_subject',
        'mail_default_intro',
        'mail_default_wanted_list',
        'mail_default_message'
    ];
}
