<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Route;
use Auth;

class DealerAddRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $post_patch_rules = array(
            'name' => 'required',
            'company' => 'required|unique:users,company,' . $this->input('id'),
        );

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                $rules = array();

                if(Auth::user()->hasRole('admin') && Route::getFacadeRoot()->current()->uri() !== 'admin/profile'){
                    $rules['email'] = 'required|unique:users,email';
                    $rules['password'] = 'required';
                }

                return array_merge($rules, $post_patch_rules);
            }
            case 'PUT':
            case 'PATCH': {
                $rules = array();

                $rules['email'] = 'required|unique:users,email,' . $this->input('id');

                return array_merge($rules, $post_patch_rules);
            }

            default:
                break;
        }
    }

    public function messages()
    {
        $messages = [
            'required' => 'This field is required.'
        ];

        return $messages;
    }
}