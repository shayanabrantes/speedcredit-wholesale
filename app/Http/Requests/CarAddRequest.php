<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class CarAddRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $car_id = $this->request->get('id');
        $post_patch_rules = array(
            'chassis_no' => 'required|unique:cars,chassis_no,' . $car_id . ',id,user_id,' . Auth::user()->id,
            'yom' => 'integer|digits:4',
            'make' => 'required',
            'model' => 'required',
            'colour' => 'required',
            'price' => 'required|numeric',
            'remarks' => 'max:255'
        );

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                $rules = [
//                    'chassis_no' => 'required|string|unique:cars',
                ];

                if (Auth::user()->hasRole('admin')) {
                    $rules['dealer'] = 'required';
                }

                return array_merge($rules, $post_patch_rules);
            }
            case 'PUT':
            case 'PATCH': {
                $rules = [
//                    'chassis_no' => 'required|unique:cars,' . $this->input('id'),
                ];

//            if(Auth::user()->hasRole('admin')){
//                $rules['user.email'] = 'required|email';
//            }

                return array_merge($rules, $post_patch_rules);
            }
            default:
                break;
        }
    }

    public function messages()
    {
        $messages = [
            'required' => 'This field is required.'
        ];

        return $messages;
    }
}