<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class CarUploadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array();

        foreach ($this->request->get('car') as $index => $row) {
            foreach ($row as $header => $value) {
                if ($header == 'dealer') {
                    $rules['car.' . $index . '.' . $header] = 'required|string';
                } else if ($header == 'chassis_no') {
                    $rules['car.' . $index . '.' . $header] = 'required|string';
//                    $rules['car.' . $index . '.' . $header] = 'required|string|unique:cars,' . Auth::user()->id . 'user_id';
                } else if ($header == 'yom') {
                    $rules['car.' . $index . '.' . $header] = 'integer|digits:4';
                } else if ($header == 'make') {
                    $rules['car.' . $index . '.' . $header] = 'required';
                } else if ($header == 'model') {
                    $rules['car.' . $index . '.' . $header] = 'required';
                } else if ($header == 'colour') {
                    $rules['car.' . $index . '.' . $header] = 'required';
                } else if ($header == 'price') {
                    if($value !== '-') {
                    $rules['car.' . $index . '.' . $header] = 'required|numeric|min:0';
                    }
                    else {
                        $rules['car.' . $index . '.' . $header] = 'required|min:0';
                    }
                } else if ($header == 'remarks') {
                    $rules['car.' . $index . '.' . $header] = 'max:255';
                } else if ($header == 'u_date') {
//                    $rules['car.' . $index . '.' . $header] = 'date';
                    $rules['car.' . $index . '.' . $header] = 'date_format:"d/m/Y"';
                }
            }
        }

        return $rules;
    }

    public function messages()
    {
        $messages = array();

        foreach ($this->request->get('car') as $index => $row) {
            $rowNumber = $index + 1;

            foreach ($row as $header => $value) {
                if ($header == 'dealer') {
                    $messages['car.' . $index . '.' . $header . '.required'] =
                        'The dealer field is required on row ' . ($rowNumber);
                } else if ($header == 'chassis_no') {
                    $messages['car.' . $index . '.' . $header . '.required'] =
                        'The chassis number field is required on row ' . ($rowNumber);

                    $messages['car.' . $index . '.' . $header . '.unique'] =
                        'A different car dealer has already uploaded a car with the same chassis number on row ' . ($rowNumber);
                } else if ($header == 'yom') {
                    $messages['car.' . $index . '.' . $header . '.required'] =
                        'The year of manufacture field is required on row ' . ($rowNumber);

                    $messages['car.' . $index . '.' . $header . '.integer'] =
                        'The year of manufacture field must be a number on row ' . ($rowNumber);

                    $messages['car.' . $index . '.' . $header . '.digits'] =
                        'The year of manufacture field must only have 4 digits on row ' . ($rowNumber);
                } else if ($header == 'make') {
                    $messages['car.' . $index . '.' . $header . '.required'] =
                        'The make field is required on row ' . ($rowNumber);
                } else if ($header == 'model') {
                    $messages['car.' . $index . '.' . $header . '.required'] =
                        'The model field is required on row ' . ($rowNumber);
                } else if ($header == 'colour') {
                    $messages['car.' . $index . '.' . $header . '.required'] =
                        'The colour field is required on row ' . ($rowNumber);
                } else if ($header == 'price') {
                    $messages['car.' . $index . '.' . $header . '.required'] =
                        'The price field is required on row ' . ($rowNumber);

                    $messages['car.' . $index . '.' . $header . '.integer'] =
                        'The wholesale price field must be a number on row ' . ($rowNumber);
                } else if ($header == 'remarks') {
                    $messages['car.' . $index . '.' . $header . '.max'] =
                        'The remarks field must only contain a maximum of 255 characters on row ' . ($rowNumber);
                } else if ($header == 'u_date') {
//                    $messages['car.' . $index . '.' . $header . '.date'] =
//                        'The u. date field must be a date on row ' . ($rowNumber);

                    $messages['car.' . $index . '.' . $header . '.date_format'] =
                        'The u. date field must be a date (DD/MM/YYYY) on row ' . ($rowNumber);
                }
            }
        }

        return $messages;
    }
}