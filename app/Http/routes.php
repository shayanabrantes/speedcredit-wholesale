<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

/* GENERAL */
Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home']);
Route::get('/jesse-tan-3333', ['uses' => 'CarsController@showJesseListing', 'as' => 'cars.jesse.listing']);

/* AJAX */
Route::get('/cars/get-dealers', ['uses' => 'CarsController@getDealers', 'as' => 'ajax.get.dealers']);
Route::get('/cars/get-makes', ['uses' => 'CarsController@getMakes', 'as' => 'ajax.get.makes']);
Route::get('/cars/get-models', ['uses' => 'CarsController@getModels', 'as' => 'ajax.get.models']);
Route::get('/cars/get-colours', ['uses' => 'CarsController@getColours', 'as' => 'ajax.get.colours']);

Route::get('/car-listing', ['uses' => 'CarsController@showPublicListing', 'as' => 'cars.public.listing']);

/* DEALER */
Route::group(['middleware' => ['role:dealer']], function () {
    Route::get('/dashboard', function () {
        return view('pages.dealers.dashboard');
    })->name('dealer.dashboard');

//    Route::get('/import', function () {
//        return view('pages.import');
//    });

//    Route::post('/import', ['uses' => 'ImportCarsController@getCsv', 'as' => 'dealer.import']);
//    Route::post('/import/save-csv', ['uses' => 'ImportCarsController@saveCsv', 'as' => 'dealer.import.save']);

    Route::resource('my-cars', 'CarsController');

    Route::post('/cars/save-action', ['uses' => 'CarsController@saveAction', 'as' => 'dealer.cars.save.action']);
    Route::post('/profile', ['uses' => 'DealersController@saveProfile', 'as' => 'dealer.profile.save']);
    Route::get('/profile', ['uses' => 'DealersController@getProfile']);
});

/* ADMIN */
Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function () {
    Route::get('/dashboard', function () {
        return view('pages.admin.dashboard');
    });

    Route::get('/import', function () {
        return view('pages.import');
    });

    Route::post('/import', ['uses' => 'ImportCarsController@getCsv', 'as' => 'admin.import']);
    Route::post('/import/save-csv', ['uses' => 'ImportCarsController@saveCsv', 'as' => 'admin.import.save']);

    Route::resource('cars', 'CarsController');
    Route::resource('dealers', 'DealersController');

    Route::get('/send-email', ['uses'=>'MailController@showEmailForm', 'as'=>'admin.send-email']);
    Route::post('/send-email/send', ['uses'=>'MailController@sendEmail', 'as'=>'admin.send-email.send']);

    Route::post('/cars/save-action', ['uses' => 'CarsController@saveAction', 'as' => 'admin.cars.save.action']);
    Route::post('/profile', ['uses' => 'DealersController@saveProfile', 'as' => 'admin.profile.save']);
    Route::get('/profile', ['uses' => 'DealersController@getProfile']);
    Route::get('/tools/delete-all-cars', ['uses'=>'CarsController@deleteAllCars']); // Temporary for testing. TODO: Remove this. -marc
});

/* EXPORT */
Route::get('/export/html', ['uses' => 'CarsController@exportHtml', 'as' => 'admin.export.html']);
Route::get('/export/pdf', ['uses' => 'CarsController@exportPdf', 'as' => 'admin.export.pdf']);
Route::get('/export/csv', ['uses' => 'CarsController@exportCsv', 'as' => 'export.csv']);