<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class HttpsProtocol {

    public function handle($request, Closure $next)
    {
        // Cloudflare fix
        $request->setTrustedProxies( [ $request->getClientIp() ] );

        if (!$request->secure() && App::environment() === 'production') {
            $app_subdirectory = env('APP_SUBDIRECTORY', '');
            $path = str_replace('/' . $app_subdirectory, '', $request->getRequestUri()); // Prevent duplication of main subdirectory from getRequestUri().

            return redirect()->secure($path);
        }

        return $next($request);
    }
}