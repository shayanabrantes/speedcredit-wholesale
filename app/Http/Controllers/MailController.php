<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use App\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public function showEmailForm()
    {
        $settings = \App\Settings::first();

        $default_subject = 'Consolidated Wholesales List - ' . date('d/m/Y');

        return view('pages.admin.mass_email', compact('settings', 'default_subject'));
    }

    public function sendEmail(Request $request)
    {
        //save default text to settings table - Shayan
        $settings = Settings::find(1)->update(array(
            'mail_default_subject' => $request->get('subject'),
            'mail_default_intro' => $request->get('intro'),
            'mail_default_wanted_list' => $request->get('wanted'),
            'mail_default_message' => $request->get('message')
        ));

        $this->validate($request, [
            'to' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);

        Mail::send('emails.mass_email', array(
            'intro' => $request->get('intro'),
            'wanted' => $request->get('wanted'),
            'message_body' => $request->get('message')
        ), function ($m) use ($request) {
            // From
            $m->from('wholesale@speedcredit.com.sg', 'Speed Credit Wholesale');

            // To
            if ($request->to == 'all') {
                $to = 'wholesale_list@speedcredit.com.sg';
            } else if ($request->to == 'temp') {
                $to = 'wholesale_temp@speedcredit.com.sg';
            } else if ($request->to == 'test') {
                $to = 'wholesale_test@speedcredit.com.sg';
            } else {
                $to = 'wholesale_test_dev@speedcredit.com.sg';
            }
            $m->to($to)->subject($request->get('subject'));

            // Attachment
            $attachment = $request->file('attachment');
            if ($attachment) {
                $m->attach($attachment->getRealPath(), array(
                    'as' => $attachment->getClientOriginalName(),
                    'mime' => $attachment->getMimeType())
                );
            }

            // Extra Attachments
            // TODO: Remove extra attachments. We should be allowed to attach more than two attachments. -marc
            $attachment_2 = $request->file('attachment_2');
            if ($attachment_2) {
                $m->attach($attachment_2->getRealPath(), array(
                        'as' => $attachment_2->getClientOriginalName(),
                        'mime' => $attachment_2->getMimeType())
                );
            }

            $attachment_3 = $request->file('attachment_3');
            if ($attachment_3) {
                $m->attach($attachment_3->getRealPath(), array(
                        'as' => $attachment_3->getClientOriginalName(),
                        'mime' => $attachment_3->getMimeType())
                );
            }

            $attachment_4 = $request->file('attachment_4');
            if ($attachment_4) {
                $m->attach($attachment_4->getRealPath(), array(
                        'as' => $attachment_4->getClientOriginalName(),
                        'mime' => $attachment_4->getMimeType())
                );
            }

            $attachment_5 = $request->file('attachment_5');
            if ($attachment_5) {
                $m->attach($attachment_5->getRealPath(), array(
                        'as' => $attachment_5->getClientOriginalName(),
                        'mime' => $attachment_5->getMimeType())
                );
            }
        });

        return back()->with('success', 'Your mass email has been queued for processing! It will take a few seconds to a few minutes to send depending on the size of your contact list.');
    }
}