<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use Mail;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = array(
            'email' => 'required|email|max:255|unique:users',
            'company' => 'required|max:255|unique:users',
            'name' => 'required|max:255',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        );

        $messages = array(
          'company.unique' => 'The company name has already been taken.'
        );

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'email' => $data['email'],
            'company' => $data['company'],
            'name' => $data['name'],
            'password' => bcrypt($data['password']),
        ]);

        $user->attachRole(2); //id of Car Dealer role - Shayan

        Mail::send('emails.welcome', ['user' => $user, 'password' => $data['password']], function ($m) use ($user) {
            $m->from('jesse.tan@speedcredit.com.sg', 'Speed Credit Wholesale');

            $m->to($user->email, $user->name)->subject('Welcome to Speed Credit Wholesale!');
        });

        return $user;
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    protected function authenticated( $user)
    {
//        if (Auth::user()->roles->first()->name == 'admin') {
//            return redirect('/admin/dashboard');
//        }

        return redirect('car-listing');
    }
}
