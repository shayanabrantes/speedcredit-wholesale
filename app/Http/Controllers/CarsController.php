<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Http\Requests;
use App\Http\Requests\CarAddRequest;
use App\Car;
use App\User;
use Auth;
use PDF;
use Excel;
use Carbon\Carbon;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::getListing();

        return view('pages.cars.cars', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.cars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarAddRequest $request)
    {
        if (Auth::user()->hasRole('admin')) {
            $dealer_name = $request->input('dealer');
            $dealer = \App\User::where('company', $dealer_name)->first();

            $car = new Car;

            if ($dealer) {
                $car->user()->associate($dealer);
            } else {
                $new_dealer = new User;
                $new_dealer->fill([
                    'company' => $dealer_name,
                    'name' => $dealer_name,
                    'email' => 'temp-' . str_random(10),
                    'password' => bcrypt(str_random(8))
                ]);
                $new_dealer->save();
                $new_dealer->attachRole(\App\Role::where('name', 'dealer')->first());

                $car->user()->associate($new_dealer);
            }

            $car->fill($request->all())->save();

            return redirect()->route('admin.cars.index')
                ->with('success', 'Added new car successfully.');
        } else {
            $dealer = $request->user();

            $car = new Car;
            $car->user()->associate($dealer);
            $car->fill($request->all())->save();

            return redirect()->route('my-cars.index')
                ->with('success', 'Added new car successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($car_id)
    {
        $car = Car::findOrFail($car_id);

        if (Auth::user()->hasRole('admin')) {
            return view('pages.cars.edit', compact('car'));
        } else {
            if (Auth::user()->id == $car->user_id)
                return view('pages.cars.edit', compact('car'));
            else
                return redirect('my-cars');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarAddRequest $request, $car_id)
    {
        if (Auth::user()->hasRole('admin')) {
//            $dealer_email = $request->input('user.email');
//            $dealer = \App\User::where('email', $dealer_email)->first();
//
//            if($dealer) {
//                \App\Car::find($car_id)->user()->associate($dealer);
//            } else {
//                $new_dealer = new User;
//                $new_dealer->fill(['email' => $dealer_email, 'name' => 'Dealer', 'password' => bcrypt(str_random(8))]);
//                $new_dealer->save();
//                $new_dealer->attachRole(\App\Role::where('name', 'dealer')->first());
//
//                \App\Car::find($car_id)->user()->associate($new_dealer);
//            }

            Car::find($car_id)->update($request->all());

            return redirect()->route('admin.cars.index')
                ->with('success', 'Car updated successfully.');
        } else {
            Car::find($car_id)->update($request->all());

            return redirect()->route('my-cars.index')
                ->with('success', 'Car updated successfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($car_id)
    {
        Car::find($car_id)->delete();

        if (Auth::user()->hasRole('admin')) {
            $route = 'admin.cars.index';
        } else {
            $route = 'my-cars.index';
        }

        return redirect()->route($route)
            ->with('success', 'Car deleted successfully.');
    }

    // Dealers
    public function getDealers()
    {
        $data = DB::table('users')
            ->distinct()
            ->pluck('company');

        return response()->json($data);
    }

    // Makes
    public function getMakes()
    {
        $popularMakes = collect([
            'Acura', 'Alfa Romeo', 'Aston Martin', 'Audi',
            'Bentley', 'BMW', 'Bugatti', 'Buick',
            'Cadillac', 'Chevrolet', 'Chrysler', 'Citroen',
            'Dodge',
            'Ferrari', 'Fiat', 'Ford',
            'Geely', 'General Motors', 'GMC',
            'Honda', 'Hyundai',
            'Infiniti',
            'Jaguar', 'Jeep',
            'Kia',
            'Koenigsegg',
            'Lamborghini', 'Land Rover', 'Lexus',
            'Maserati', 'Mazda', 'McLaren', 'Mercedes-Benz', 'Mini', 'Mitsubishi',
            'Nissan',
            'Pagani', 'Peugeot', 'Porsche',
            'Ram', 'Renault', 'Rolls Royce',
            'Saab', 'Subaru', 'Suzuki',
            'TATA Motors', 'Tesla', 'Toyota',
            'Volkswagen', 'Volvo',
            'Mercedes', 'Mercedes benz', 'Merdeces benz', 'Benz', 'Toyta' // Common typos to fix
        ]);

        $data = $popularMakes;

//        $data = DB::table('cars')
//            ->distinct()
//            ->pluck('make');
//
//        $data = collect($data)->merge($popularMakes);

        return response()->json($data);
    }

    // Models
    public function getModels()
    {
        $data = DB::table('cars')
            ->distinct()
            ->pluck('model');

        return response()->json($data);
    }

    // Colour
    public function getColours()
    {
        $data = DB::table('cars')
            ->distinct()
            ->pluck('colour');

        return response()->json($data);
    }

    // Public Car Listing View
    public function showPublicListing()
    {
        $cars = Car::getListing('public');

        return view('pages.cars', compact('cars'));
    }

    // Jesse's Public Car Listing View
    public function showJesseListing()
    {
        $cars = Car::getListing('public');

        return view('pages.jesse', compact('cars'));
    }

    // Export PDF
    public function exportPdf(Request $request)
    {
        $cars = Car::getListing('public', $request->sort);

        $date = Carbon::now()->format('dmY');
        $title = 'Consolidated Wholesales List - ' . $date;

        $pdf = PDF::loadView('export.cars', compact('cars', 'title'));

        return $pdf->setPaper('a4', 'portrait')->download($title . '.pdf');
    }

    // Export HTML
    public function exportHtml(Request $request)
    {
        $cars = Car::getListing('public', $request->sort);

        $date = Carbon::now()->format('dmY');
        $title = 'Consolidated Wholesales List - ' . $date;

        return view('export.cars', compact('cars', 'title'));
    }

    // Export CSV
    public function exportCsv()
    {
        $columns = array(
            'chassis_no',
            'yom',
            'make',
            'model',
            'vac',
            'omv',
            'ves',
            'colour',
            'price',
            'type',
            'remarks',
            'sold'
        );

        $cars = Car::getListing();
        $car_list = array();

        if (Auth::user()->hasRole('admin')) {
            array_push($columns, 'updated_at');
            array_unshift($columns, 'user_id');
            array_unshift($columns, 'no');
            array_splice($columns, array_search('price', $columns) + 1, 0, array('wholesale_price')); // Add wholesale_price after price.

            foreach ($cars as $key => $car) {
                $data = array(
                    $car->listing_no,
                    $car->company,
                    $car->chassis_no,
                    $car->yom,
                    $car->make,
                    $car->model,
                    $car->vac,
                    $car->omv,
                    $car->cevs,
                    $car->colour,
                    $car->price,
                    getWholesalePrice($car->price),
                    $car->type,
                    $car->remarks,
                    $car->sold,
                    date_format(date_create($car->updated_at), 'd/m/Y')
                );

                array_push($car_list, $data);
            }
        } else {
            foreach ($cars as $key => $car) {
                $data = array(
                    $car->chassis_no,
                    $car->yom,
                    $car->make,
                    $car->model,
                    $car->vac,
                    $car->omv,
                    $car->cevs,
                    $car->colour,
                    $car->price,
                    $car->type,
                    $car->remarks,
                    $car->sold,
                );

                array_push($car_list, $data);
            }
        }

        $title = 'export_cars_' . Carbon::now()->format('YmdHis');

        return Excel::create($title, function ($excel) use ($columns, $car_list) {
            $excel->sheet('wholesale', function ($sheet) use ($columns, $car_list) {
                if (Auth::user()->hasRole('admin')) {
                    $columns[1] = 'dealer'; //update first column header to dealer (instead of user_id) - Shayan
                    $columns[count($columns) - 1] = 'u_date'; //update last column header to u_date (instead of updated_at) - Shayan
                }

                $sheet->fromArray($car_list, null, 'A1', false, false)->prependRow($columns);
            });
        })->export('xlsx');
    }

    public function deleteAllCars() // Temporary for testing. TODO: Remove this. -marc
    {
        Car::truncate();

        return 'Done.';
    }

    public function saveAction(Request $request)
    {
        $car_ids = $request->cars;
        $action_type = $request->actionType;
        $updated_value = $request->updatedValue;

        foreach ($car_ids as $car_id) {
            if ($action_type == 'delete') {
                Car::find($car_id)->delete();
            } else if (strpos($action_type, 'mark_as_') !== false) {
                $mark_type = substr($action_type, strpos($action_type, 'mark_as_') + 8); // 'mark_as_' == 8 characters
                if ($mark_type == 'none') {
                    Car::find($car_id)->update(array('sold' => ''));
                } else {
                    Car::find($car_id)->update(array('sold' => $mark_type));
                }
            } else if ($action_type == 'update_vac') {
                Car::find($car_id)->update(array('vac' => $updated_value));
            } else if ($action_type == 'update_price') {
                Car::find($car_id)->update(array('price' => $updated_value));
            } else if ($action_type == 'update_remarks') {
                Car::find($car_id)->update(array('remarks' => $updated_value));
            }
        } if ($action_type == 'delete') {
            $request->session()->flash('success', 'Car(s) deleted successfully.');
        } else {
            $request->session()->flash('success', 'Car(s) updated successfully.');
        }

        return response()->json(['success' => true, '']);
    }
}