<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Role;
use App\Car;
use App\User;
use App\Upload;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CarUploadRequest;
use Mockery\CountValidator\Exception;
use Validator;
use Excel;
use CsvValidator;

class ImportCarsController extends Controller
{
    public function getCsv(Request $request)
    {
        // Check if file is not empty and has the correct extension. - Shayan
        $csv = $request->file('csv');

        $validator = Validator::make(
            [
                'file' => $csv,
                'extension' => $csv ? strtolower($csv->getClientOriginalExtension()) : null
            ],
            [
                'file' => 'required',
                'extension' => 'in:csv,xls,xlsx'
            ],
            [
                'required' => 'Please upload a csv, xls, or xlsx file.',
                'in' => 'Please upload a csv, xls, or xlsx file.'
            ]
        );

        if ($validator->fails()) {
            // Return file/extension validation errors.
            return response()->json(['success' => false,
                'data' => $validator->messages()]);
        }

        // Check headers and column values. - Shayan
//        $rules = [
//            'chassis_no' => 'required|string',
//            'yom' => 'required|integer',
//            'make' => 'required|string',
//            'model' => 'required|string',
//            'colour' => 'string',
//            'price' => 'required|numeric',
//            'remarks' => 'string',
//        ]; // TODO: Fix rules. -marc

        $rules = [];

        try {
            $csv_validator = CsvValidator::make($csv, $rules);

            if ($csv_validator->fails()) {
                $file_errors = array();
                $errors = $csv_validator->getErrors();

                foreach ($errors as $row_index => $error) {
                    foreach ($error as $col_index => $messages) {
                        $message = 'Row ' . $row_index . ', Column ' . $col_index . ': ' . implode(',', $messages);
                        array_push($file_errors, $message);
                    }
                }

                // CSV row/column errors.
                return response()->json(['success' => false,
                    'data' => $file_errors]);
            } else {
                $raw_content = Excel::load($csv)->formatDates(false)->toArray();
                $content = array();

                $trim = function ($value) {
                    if (is_string($value)) {
                        $value = trim($value);
                    }

                    return $value;
                };

                foreach ($raw_content as $row) {
                    $content[] = array_map($trim, $row);
                }

                $rowCount = count($content);
                $columnCount = count($content[0]);

                if (($rowCount * $columnCount) <= ini_get("max_input_vars")) {
                    return response()->json(['success' => true,
                        'data' => $content]);
                } else {
                    return response()->json(['success' => false,
                        'data' => array('The file you uploaded is too large. Please try decreasing the number of cars being uploaded and try again.')]);
                }

            }
        } catch (\Exception $e) {
            // CSV heading or formatting errors.
            return response()->json(['success' => false,
                'data' => array($e->getMessage())]);
        }
    }

    public function saveCsv(CarUploadRequest $request)
    {
        $dealer_names = array();
        $record_update_count = 0;
        $record_save_count = 0;

        $user = Auth::user();

        // Overwrite All
        if ($request->get('overwrite_all')) {
            if (Auth::user()->hasRole('admin')) {
                //
                foreach ($request->get('car') as $row) {
                    $dealer_name = $row['dealer'];

                    if (!in_array($dealer_name, $dealer_names)) {
                        $dealer_names[] = $dealer_name;
                    }
                }

                //
                foreach ($dealer_names as $dealer_name) {
                    $user = User::where('company', $dealer_name)->first();

                    if ($user) {
                        $user_id = $user->id;
                        Car::where('user_id', $user_id)->delete();
                    }
                }
            } else {
                Car::where('user_id', Auth::user()->id)->delete();
            }
        }

        // Upload Tracking
        $upload = new Upload;
        $upload->user()->associate($user);
        $upload->save();

        //
        foreach ($request->get('car') as $row) {
            $car_attributes = array();
            foreach ($row as $header => $value) {
                if ($header == 'u_date') {
                    if ($value) {
                        $car_attributes['updated_at'] = Carbon::createFromFormat('d/m/Y', $value)->toDateTimeString();
                    }
                } else {
                    $car_attributes[$header] = $value;
                }
            }

            $chassis_no = $car_attributes['chassis_no'];

            if (Auth::user()->hasRole('admin')) {
                $dealer_name = $car_attributes['dealer'];
                $dealer = User::where('company', $dealer_name)->first();

//                if (!$dealer) {
//                    $dealer = new User;
//                    $dealer->fill([
//                        'name' => $dealer_name,
//                        'company' => $dealer_name,
//                        'email' => 'temp-' . str_random(10),
//                        'password' => bcrypt(str_random(8))
//                    ]);
//                    $dealer->save();
//                    $dealer->attachRole(Role::where('name', 'dealer')->first());
//                }

                $car = Car::firstOrNew([
                    'chassis_no' => $chassis_no,
                    'user_id' => $dealer->id
                ]);

                $car->user()->associate($dealer);

                if ($car->exists) {
                    $car->update($car_attributes);
                    $record_update_count++;
                } else {
                    $car->upload()->associate($upload);
                    $car->fill($car_attributes)->save();
                    $record_save_count++;
                }
            } else if (Auth::user()->hasRole('dealer')) {
                $car = Car::firstOrNew([
                    'chassis_no' => $chassis_no,
                    'user_id' => Auth::user()->id
                ]);

                if ($car->exists) {
                    $car->update($car_attributes);
                    $record_update_count++;
                } else {
                    $car->user()->associate($user);
                    $car->upload()->associate($upload);
                    $car->fill($car_attributes)->save();
                    $record_save_count++;
                }
            }
        }

        return response()->json(['success' => true,
            'data' => array('recordUpdateCount' => $record_update_count, 'recordSaveCount' => $record_save_count)]);
    }
}