<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;
use Auth;
use App\Http\Requests;
use App\User;
use App\Role;
use App\Http\Requests\DealerAddRequest;

class DealersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dealers = User::whereHas('roles', function ($q) {
            $q->where('name', 'dealer');
        })->orderBy('created_at', 'desc')->get();

        return view('pages.dealers.dealers', compact('dealers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.dealers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DealerAddRequest $request)
    {
        $dealer = new User;
        $dealer->fill(['email' => $request->email, 'company' => $request->company, 'name' => $request->name, 'password' => bcrypt($request->password)])->save();
        $dealer->attachRole(Role::where('name', 'dealer')->first());
        if ($request->input('send_email')){
            $user = User::findOrFail($dealer->id);
            Mail::send('emails.credentials', array(
                'user' => $user,
                'password' => $request->input('password')
            ), function ($m) use ($user) {
                $m->from('jesse.tan@speedcredit.com.sg', 'Speed Credit Wholesale');
                $m->to($user->email, $user->name)->subject('Speed Credit Wholesale - Your Credentials');
            });
        }

        return redirect()->route('admin.dealers.index')
            ->with('success', 'Added new dealer successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($dealer_id)
    {
        $dealer = User::findOrFail($dealer_id);

        return view('pages.dealers.edit', compact('dealer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DealerAddRequest $request, $dealer_id)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email,' . $dealer_id,
        ]);

        User::find($dealer_id)->update($request->except('password'));

        if ($request->input('password')) {
            User::find($dealer_id)->update(array('password' => bcrypt($request->input('password'))));

            if ($request->input('send_email')){
                $user = User::findOrFail($dealer_id);

                Mail::send('emails.credentials', array(
                    'user' => $user,
                    'password' => $request->input('password')
                ), function ($m) use ($user) {
                    $m->from('jesse.tan@speedcredit.com.sg', 'Speed Credit Wholesale');
                    $m->to($user->email, $user->name)->subject('Speed Credit Wholesale - Your Credentials');
                });
            }
        }

        return redirect()->route('admin.dealers.index')
            ->with('success', 'Dealer updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($dealer_id)
    {
        User::find($dealer_id)->delete();

        return redirect()->route('
        admin.dealers.index')
            ->with('success', 'Dealer deleted successfully.');
    }

    public function getProfile()
    {
        $dealer_id = Auth::user()->id;
        $dealer = User::findOrFail($dealer_id);

        return view('pages.dealers.edit', compact('dealer'));
    }

    public function saveProfile(DealerAddRequest $request)
    {
        $dealer_id = Auth::user()->id;

        $this->validate($request, [
            'email' => 'required|unique:users,email,' . $request->input('id')
        ]);

        User::find($dealer_id)->update($request->except('password'));

        if ($request->input('password')) {
            User::find($dealer_id)->update(array('password' => bcrypt($request->input('password'))));
        }

        return redirect()->back()->with('success', 'Profile updated successfully.');
    }
}