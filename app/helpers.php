<?php

function format_value($field_name, $field_value, $is_sold = null)
{
    $formatted_value = $field_value;

    if (empty($field_value)) {
        $formatted_value = '';
    }

    if ($field_name == 'price' && strtolower($is_sold) == 'y') {
        $formatted_value = '';
    }

    if ($field_name == 'type' && empty($field_value)) {
        $formatted_value = 'Private';
    }

    return $formatted_value;
}

function update_sold_value($is_sold, $price)
{
    if (strtolower($is_sold) == 'y') {
        return $price;
    }

    return null;
}

function getWholesalePrice($original_price)
{
    return $original_price + 1000;
}