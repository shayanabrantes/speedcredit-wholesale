<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Auth;

class Car extends Model
{
    protected $fillable = [
        'chassis_no',
        'yom',
        'make',
        'model',
        'vac',
        'omv',
        'cevs',
        'colour',
        'price',
        'type',
        'remarks',
        'sold',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function upload()
    {
        return $this->belongsTo('App\Upload');
    }

    public static function getListing($type = null, $sort = null)
    {
        DB::select('SET @i := 0'); // Initialize listing_no counter.

        $order_by = '
            cars.make,
            cars.model,
            field(cars.yom, "", "-"),
            cars.yom,
            cars.colour
        ';

        if ($sort == 'u_date') {
            $order_by = 'cars.updated_at DESC,' . $order_by;
        }

        $cars = DB::table('cars')
            ->select(DB::raw('
                cars.*,
                @i := @i + 1 AS listing_no,
                users.company
            '))
            ->leftJoin('users', 'users.id', '=', 'cars.user_id')
            ->orderByRaw($order_by);

        if (Auth::user() && $type != 'public') {
            if (Auth::user()->hasRole('dealer')) {
                $cars->where('user_id', '=', Auth::user()->id);
            }
        }

        return $cars->get();
    }
}