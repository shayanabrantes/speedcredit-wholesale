<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('upload_id')->nullable();
            $table->foreign('upload_id')->references('id')->on('uploads')->onDelete('cascade');
            $table->string('chassis_no');
            $table->string('yom');
            $table->string('make');
            $table->string('model');
            $table->string('vac');
            $table->decimal('omv', 10, 2);
            $table->integer('cevs');
            $table->string('colour');
            $table->decimal('price', 10, 2);
            $table->string('type');
            $table->text('remarks')->nullable();
            $table->string('sold');
            $table->string('sold_sc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cars');
    }
}