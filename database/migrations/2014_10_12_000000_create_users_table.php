<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company');
            $table->string('name');
            $table->string('tel_no');
            $table->string('mobile_no');
            $table->string('fax');
            $table->string('business_reg_no');
            $table->string('address1');
            $table->string('address2');
            $table->string('postal_code');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('email_frequency')->default('none');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}