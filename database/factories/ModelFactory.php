<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {

    $roles = array('Admin', 'Dealer', 'Viewer');

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'role' => array_rand($roles),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Dealer::class, function (Faker\Generator $faker) {
    return [
        'dealer_name' => $faker->company,
    ];
});

$factory->define(App\Car::class, function (Faker\Generator $faker) {
    return [
        'dealer_id' => factory(App\Dealer::class)->create()->id,
        'chassis_no' => $faker->randomNumber(5),
        'year_of_manufacture' => $faker->year,
        'maker' => $faker->word,
        'model' => $faker->company,
        'colour' => $faker->colorName,
        'remarks' => $faker->text,
    ];
});
