<?php

use Illuminate\Database\Seeder;
use App\Settings;

class SettingsTableSeeder extends Seeder
{
    public function run()
    {
        $settings = new Settings;

        $settings->mail_default_subject = 'Consolidated Wholesales List - ' . date('d/m/Y');

        $settings->mail_default_intro = '<div>Dear Business Associates,</div>';

        $settings->mail_default_wanted_list = '';

        $settings->mail_default_message = '
            <div><span style="background: #ffff00;">Anyone who wish to provide me your car list for wholesale can use the attached to email back to me.<br>WhatsApp me anytime to sell off your TCOE.</span></div>
            <div>&nbsp;</div>
            <div>This info can be receive by <span style="background: #ffff00;">email or WhatsApp</span>.<br>If you wish to receive this info or contribute to my list, please WhatsApp me <span style="background: #ffff00;">(9618 3333)</span> or email <span style="background: #ffff00;">(jesse.tan@speedcredit.com.sg)</span> me your preferred communication mode.<br>Attached you will find the wholesales price list in <span style="background: #ffff00;">(PDF)</span>.<br> <br>This is a consolidated stock list from many car dealers.<br>Stock movement will be fast.<br>Please double check with me before any confirmation.<br>Information shown may not be 100% accurate.<br>Final vehicle details will base on details shown in invoice.<br>You are responsible to check the details yourself.<br>Some prices are negotiable.</div>
            <div>&nbsp;</div>
            <div>If you have any stock, car body and/or TCOE to offer or wanted, please email me or WhatsApp me , I can place them in the offer list below. <br><span style="background: #ffff00;">(jesse.tan@speedcredit.com.sg) (9618 3333)</span></div>
            <div>&nbsp;</div>
            <div>The information I need are as follows:</div>
            <div>&nbsp;</div>
            <div>1. YOM.<br>2. Last 4 digit of chassis no. (optional).<br>3. Make &amp; Model.<br>4. OMV.<br>5. Colour.<br>6. Price.<br> <br>TCOE.<br>1. Selling price for the TCOE.<br>2. Expiry date.<br>3. TCOE value bidded.</div>
            <div>&nbsp;</div>
            <div>Car Body<br>1. Make &amp; Model.<br>2. Price.</div>
            <div>&nbsp;</div>
            <div>Please provide me with as much info I need as possible, so that I can help you match your need faster.<br>If you see your info provide to me is not correct, please inform me.<br> <br>Prices are subject to change without prior notice.<br>Stocks are subject to change without prior notice.<br>Phone reservation will not be entertain.<br>Return of faxed invoice can hold the stock for only 1 day, and must follow up by payment the next day.<br>Only upon receiving a deposit that a stock will be confirmed reserved for a week.<br>If need to wait for more than a week, please specified and special arrangement will be make.<br>Vehicle will only be released upon receiving full payment.<br>$500 penalty for any cancellation after receiving signed confirmation from you.</div>
        ';

        $settings->save();
    }
}