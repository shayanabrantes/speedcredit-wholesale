<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

//        $dealer_role = Role::where('name', 'dealer')->first();
//
//        for ($i = 1; $i <= 10; $i++) {
//            $dealer_email = 'sc' . $i . '@dyagonet.com';
//
//            $dealer_user = new User;
//            $dealer_user->name = $faker->name;
//            $dealer_user->company = $faker->company;
//            $dealer_user->tel_no = '12345678';
//            $dealer_user->mobile_no = '12345678';
//            $dealer_user->fax = '12345678';
//            $dealer_user->business_reg_no = '12345567890';
//            $dealer_user->address1 = $faker->address;
//            $dealer_user->postal_code = '123456';
//            $dealer_user->email = $dealer_email;
//            $dealer_user->password = bcrypt('12345678');
//            $dealer_user->save();
//
//            $dealer = User::where('email', $dealer_email)->first();
//            $dealer->attachRole($dealer_role);
//        }

        $admin_role = Role::where('name', 'admin')->first();

        $admin_email = 'admin@dyagonet.com';

        $admin_user = new User;
        $admin_user->name = 'Jesse Tan';
        $admin_user->email = $admin_email;
        $admin_user->password = bcrypt('12345678');
        $admin_user->save();

        $admin = User::where('email', $admin_email)->first();
        $admin->attachRole($admin_role);
    }
}