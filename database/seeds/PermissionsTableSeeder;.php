<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $importCsv = new Permission();
        $importCsv->name = 'import-csv';
        $importCsv->display_name = 'Import CSV'; // optional
        $importCsv->description = 'Import CSV, XLS, or XLSX files'; // optional
        $importCsv->save();

        $saveCsv = new Permission();
        $saveCsv->name = 'save-csv';
        $saveCsv->display_name = 'Save CSV'; // optional
        $saveCsv->description = 'Save imported CSV, XLS, or XLSX files'; // optional
        $saveCsv->save();

        $dealer = Role::where('name', 'dealer')->first();

        $dealer->attachPermissions(array($importCsv, $saveCsv));
    }
}