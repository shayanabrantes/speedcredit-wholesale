<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name = 'admin';
        $admin->display_name = 'Administrator'; // optional
        $admin->save();

        $dealer = new Role();
        $dealer->name = 'dealer';
        $dealer->display_name = 'Car Dealer'; // optional
        $dealer->save();
    }
}