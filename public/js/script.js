(function ($) {
    $(function () {
        var lastChecked = null;
        var $checkboxes = $('input[type=checkbox]');

        // DataTables dd/mm/yyyy sorting
        $.extend(jQuery.fn.dataTableExt.oSort, {
            'extract-date-pre': function (value) {
                var date = value;
                date = date.split('/');
                return Date.parse(date[1] + '/' + date[0] + '/' + date[2])
            },
            'extract-date-asc': function (a, b) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },
            'extract-date-desc': function (a, b) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        });

        // Non case-sensitive inArray
        $.extend({
            inArrayIn: function (elem, arr, i) {
                if (typeof elem !== 'string') {
                    return $.inArray.apply(this, arguments);
                }

                if (arr) {
                    var len = arr.length;
                    i = i ? (i < 0 ? Math.max(0, len + i) : i) : 0;
                    elem = elem.toLowerCase();
                    for (; i < len; i++) {
                        if (i in arr && arr[i].toLowerCase() == elem) {
                            return i;
                        }
                    }
                }

                return -1;
            }
        });

        // DataTable Column Search
        $('table.sc-datatable.column-search').each(function () {
            var $this = $(this);
            var html = '';

            html += '<tr class="search">';

            $('thead > tr th', $this).each(function () {
                var title = $(this).text();
                var searchInput = '<input style="width: 100%;" type="text" placeholder="Search ' + title + '"/>';

                if ($(this).hasClass('sc-no-search')) {
                    html += '<th><span style="display: none;">' + searchInput + '</span></th>';
                } else {
                    html += '<th>' + searchInput + '</th>';
                }
            });

            html += '</tr>';

            $this.find('thead tr').eq(0).before(html);
        });

        // DataTable
        $('table.sc-datatable').each(function () {
            var $this = $(this);
            var noColumnIndex = null;
            var dateColumnIndex = null;
            var noSortColumnIndex = [];

            $this.find('thead th').each(function () { // TODO: Support multiple classes. -marc
                if ($(this).hasClass('sc-no')) {
                    noColumnIndex = $(this)[0].cellIndex;
                }

                if ($(this).hasClass('sc-u-date')) {
                    dateColumnIndex = $(this)[0].cellIndex;
                }

                if ($(this).hasClass('sc-no-sort')) {
                    noSortColumnIndex.push($(this)[0].cellIndex);
                }
            });

            var $dataTable = $this.DataTable({
                aLengthMenu: [
                    [25, 50, 100, 200, -1],
                    [25, 50, 100, 200, "All"]
                ],
                iDisplayLength: 50,
                dom: '<"row"<"col-xs-12"f>>' +
                '<"row"<"col-xs-12 col-lg-2"l><"col-xs-12 col-lg-10"p>>' +
                '<"row"<"col-xs-12"tr>>' +
                '<"row"<"col-xs-12 col-lg-2"l><"col-xs-12 col-lg-10"p>>' +
                '<"row"<"col-xs-12"i>>',
                searchDelay: 500,
                orderClasses: false,
                fixedHeader: true,
                order: [noColumnIndex, 'asc'],
                columnDefs: [
                    {
                        type: 'extract-date',
                        orderData: [dateColumnIndex],
                        targets: [dateColumnIndex]
                    },
                    {
                        orderData: [dateColumnIndex, noColumnIndex],
                        targets: [dateColumnIndex]
                    }, {
                        orderable: false,
                        targets: noSortColumnIndex
                    }
                ]
            });

            //
            if (window.location.search.indexOf('sort=u_date') > -1) {
                $dataTable.order([dateColumnIndex, 'desc']).draw();
            } else if (window.location.search.indexOf('sort=default') > -1) {
                $dataTable.order([noColumnIndex, 'asc']).draw();
            }

            // DataTable Column Search Separator
            $dataTable.columns().every(function () {
                var that = this;

                $('thead tr.search input', $this).eq(this.index()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        var regExSearch = false;
                        var smartSearch = true;

                        if (this.value.indexOf('|') >= 0) {
                            regExSearch = true;
                            smartSearch = false;
                        }

                        that.search(this.value, regExSearch, smartSearch).draw();
                    }
                });
            });
        });

        //
        $('.dataTables_filter input[type="search"]').on('keyup click', function () {
            var regExSearch = false;
            var smartSearch = true;

            if ($(this).val().indexOf('|') >= 0) {
                regExSearch = true;
                smartSearch = false;
            }

            $('table.sc-datatable').DataTable().search($(this).val(), regExSearch, smartSearch).draw();
        });

        //
        $('.dataTable').on('draw.dt', function () {
            lastChecked = null;
            $checkboxes = $('.dataTable input[type=checkbox]');
        });

        // TinyMCE
        tinymce.init({
            selector: 'textarea.tiny-mce',
            branding: false,
            statusbar: false,
            plugins: [
                'autolink lists link',
                'searchreplace'
            ],
            style_formats: [
                {title: 'Highlight', inline: 'span', styles: {background: '#ffff00', color: '#000'}}
            ],
            forced_root_block: 'div'
        });

        // Checkbox using shift key
        $checkboxes.click(function (e) {
            if (!lastChecked) {
                lastChecked = this;
                return;
            }

            if (e.shiftKey) {
                var start = $checkboxes.index(this);
                var end = $checkboxes.index(lastChecked);
                $checkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', lastChecked.checked);
            }

            lastChecked = this;
        });

        // Check All Checkbox
        $('body').on('click', '.check-all:checkbox', function () {
            $(this).closest('table').find('tbody input:checkbox').prop('checked', this.checked).trigger('change');
        });
    });
})(jQuery);

// Random String
function randomString(length, chars) {
    var result = '';

    if (!chars) {
        chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }

    for (var i = length; i > 0; --i) {
        result += chars[Math.floor(Math.random() * chars.length)];
    }

    return result;
}

// YYMMDD Date
Date.prototype.yymmdd = function () {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear().toString().substr(-2),
        (mm > 9 ? '' : '0') + mm,
        (dd > 9 ? '' : '0') + dd
    ].join('');
};